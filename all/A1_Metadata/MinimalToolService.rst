.. _minimalToolService:

Minimal elements for tools/services
###################################

This page describes the minimal metadata elements specific to **tools/services**.

----

ToolService
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Mandatory

:guilabel:`Explanation & Instructions`

Introduces the set of elements that is specific to tools/services

:guilabel:`Example`

.. code-block:: xml

	<ms:LRSubclass>
		<ms:ToolService>
			<ms:lrType>toolService</ms:lrType>
			...
		</ms:ToolService>
	</ms:LRSubclass>

----

function
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService.function``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Mandatory

:guilabel:`Explanation & Instructions`

Specifies the operation/function/task that a software object performs

The element is important for discovery purposes.
You can fill in:

- the ``LTClassRecommended`` element with one of the recommended values from the `LT taxonomy <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#LTClassRecommended>`_, or
- the ``LTClassOther`` element with a free text.

For services that perform multiple functions (e.g., syntactic and semantic annotation) you can repeat the element.


:guilabel:`Example`

.. code-block:: xml

	<ms:function>
		<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/NamedEntityRecognition</ms:LTClassRecommended>
	</ms:function>

	<ms:function>
		<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation</ms:LTClassRecommended>
	</ms:function>

	<ms:function>
		<ms:LTClassOther>video segmentation</ms:LTClassRecommended>
	</ms:function>

----

SoftwareDistribution
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService.SoftwareDistribution``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Mandatory

:guilabel:`Explanation & Instructions`

Any form with which software is distributed (e.g., web services, executable or code files, etc.)

This element groups together information that pertains to the physical form of a tool/service that is made available through the catalogue.
For software that is distributed with multiple forms (e.g., as source code, as a web service, etc.), you can repeat this group of elements. The access location and the licensing conditions may differ for each distribution.

The following list includes the mandatory and recommended elements:

- ``SoftwareDistributionForm`` (Mandatory): The medium, delivery channel or form (e.g., source code, API, web service, etc.) through which a software object is distributed. Use the value ``http://w3id.org/meta-share/meta-share/dockerImage``.
- ``dockerDownloadLocation`` (Mandatory if applicable): A location where the the LT tool docker image is stored.  Add the location from where the ELG team can download the docker image in order to test it.
- ``serviceAdapterDownloadLocation`` (Mandatory if applicable): Τhe URL where the docker image of the service adapter can be downloaded from. Required only for ELG functional services implemented with an adapter.
- ``executionLocation`` (Mandatory): A URL where the resource (mainly software) can be directly executed. Add here the REST endpoint at which the LT tool is exposed within the Docker image.
- ``additionalHwRequirements`` (Mandatory if applicable): A short text where you specify additional requirements for running the service, e.g. memory requirements, etc. The recommended format for this is: 'limits_memory: X limits_cpu: Y'
- ``licenceTerms`` (Mandatory): See :ref:`licence`

----

.. _licence:

licenceTerms
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService.SoftwareDistribution.licenceTerms``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Mandatory

:guilabel:`Explanation & Instructions`

Links the distribution (distributable form) of a language resource to the licence or terms of use/service (a specific legal document) with which it is distributed

The recommended practice is to add a licence name and identifier from the SPDX list of licences (https://spdx.org/licenses/). For proprietary licences or licences not included in the above list, please add a (unique) licence name and the URL where the text of the licence can be found.

:guilabel:`Example`

.. code-block:: xml

	<ms:licenceTerms>
		<ms:licenceTermsName xml:lang="en">GNU Lesser General Public License v3.0 only</ms:licenceTermsName>
		<ms:licenceTermsURL>https://spdx.org/licenses/LGPL-3.0-only.html</ms:licenceTermsURL>
		<ms:LicenceIdentifier ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX">LGPL-3.0-only</ms:LicenceIdentifier>
	</ms:licenceTerms>

	<ms:licenceTerms>
		<ms:licenceTermsName xml:lang="en">publicDomain</ms:licenceTermsName>
		<ms:licenceTermsURL>https://elrc-share.eu/terms/publicDomain.html</ms:licenceTermsURL>
	</ms:licenceTerms>

	<ms:licenceTerms>
		<ms:licenceTermsName xml:lang="en">Creative Commons Attribution 4.0 International</ms:licenceTermsName>
		<ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/legalcodel</ms:licenceTermsURL>
		<ms:LicenceIdentifier ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX">CC-BY-4.0</ms:LicenceIdentifier>
	</ms:licenceTerms>

----

languageDependent
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService.languageDependent``

:guilabel:`Data type`	boolean

:guilabel:`Optionality`	Mandatory

:guilabel:`Explanation & Instructions`

Indicates whether the operation of the tool or service is language dependent or not

For language-dependent tools/services, you will be asked to also provide the language of the input and output resources.

:guilabel:`Example`

.. code-block:: xml

	<ms:languageDependent>true</ms:languageDependent>

----

.. _inputResource:

inputContentResource
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService.inputContentResource``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Mandatory

:guilabel:`Explanation & Instructions`

Specifies the requirements set by a tool/service for the (content) resource that it processes

The following elements are mandatory or recommended:

- ``processingResourceType`` (Mandatory): Specifies the resource type that a tool/service takes as input or produces as output; you must specify, for instance, if the tool/service can process a single file, or set of files, or processes a string typed in by the users.
- ``language`` (Mandatory if applicable): Specifies the language that is used in the resource or supported by the tool/service, expressed according to the BCP47 recommendation. See :ref:`language`
- ``mediaType`` (Recommended): Specifies the media type of the input/output of a language processing tool/service. For ELG functional services, this will be used to fit the appropriate GUI (e.g. "audio" for ASR applications, vs. "text" for Machine Translation applications)
- ``dataFormat`` (Recommended): Indicates the format(s) of a data resource	Please, use to indicate the data format of the resource supported by the tool/service. 	The `dataFormat <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#dataFormat>`_ controlled vocabulary lists data formats, with their mimetype and documentation on the particularities, thus catering for variations of formats, e.g. GATE XML, TEI variants, etc.
- ``characterEncoding`` (Recommended if applicable): Specifies the character encoding used for the input/output text resource	of an LT service
- ``annotationType`` (Recommended if applicable): Specifies the annotation type of the annotated version(s) of a resource or the annotation type a tool/ service requires or produces as an output. Use this element only if the tool/service processes pre-annotated corpora; for tools/services processing raw files, do not use. The element takes a value from a controlled vocabulary, see `annotationType <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHAREschema.html#annotationType>`_.


:guilabel:`Example`

.. code-block:: xml

	<!-- example for a tool with textual input -->
	<ms:inputContentResource>
		<ms:processingResourceType>http://w3id.org/meta-share/meta-share/file1</ms:processingResourceType>
		<ms:language>
			<ms:languageTag>en</ms:languageTag> <ms:languageId>en</ms:languageId>
		</ms:language>
		<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
		<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Json</ms:dataFormat>
		<ms:characterEncoding>http://w3id.org/meta-share/meta-share/UTF-8</ms:characterEncoding>
	</ms:inputContentResource>

	<!-- example for an Automatic Speech Recognizer -->
	<ms:inputContentResource>
		<ms:processingResourceType>http://w3id.org/meta-share/meta-share/file1</ms:processingResourceType>
		<ms:language>
			<ms:languageTag>de</ms:languageTag> <ms:languageId>de</ms:languageId>
		</ms:language>
		<ms:mediaType>http://w3id.org/meta-share/meta-share/audio</ms:mediaType>
		<ms:dataFormat>http://w3id.org/meta-share/omtd-share/mp3</ms:dataFormat>
		<ms:dataFormat>http://w3id.org/meta-share/omtd-share/wav</ms:dataFormat>
	</ms:inputContentResource>

----

outputResource
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService.outputResource``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Recommended if applicable

:guilabel:`Explanation & Instructions`

Describes the features of the output resource processed by a tool/service.

The set of elements are the same as for the :ref:`inputResource`.

Make sure that you add here what is relevant for your application. For instance,

- for annotation and information extraction tools/services, use the ``annotationType`` to indicate the results of your processing; you can repeat it to indicate mutliple annotation types (e.g., part of speech, person, amount, location, etc.)
- for Machine Translation tools, indicate the input and output languages respectively.

:guilabel:`Example`

.. code-block:: xml

	<!-- example for an Information Extraction tool -->
	<ms:outputResource>
		<ms:processingResourceType>http://w3id.org/meta-share/meta-share/file1</ms:processingResourceType>
		<ms:language>
			<ms:languageTag>en</ms:languageTag>
			<ms:languageId>en</ms:languageId>
		</ms:language>
		<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
		<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Json</ms:dataFormat>
		<ms:characterEncoding>http://w3id.org/meta-share/meta-share/UTF-8</ms:characterEncoding>
		<ms:annotationType>http://w3id.org/meta-share/omtd-share/Person</ms:annotationType>
		<ms:annotationType>http://w3id.org/meta-share/omtd-share/Location</ms:annotationType>
		<ms:annotationType>http://w3id.org/meta-share/omtd-share/Organization</ms:annotationType>
		<ms:annotationType>http://w3id.org/meta-share/omtd-share/Date</ms:annotationType>
	</ms:outputResource>

	<!-- example for a Machine Translation tool -->
	<ms:outputResource>
		<ms:processingResourceType>http://w3id.org/meta-share/meta-share/file1</ms:processingResourceType>
		<ms:language>
			<ms:languageTag>en</ms:languageTag>
			<ms:languageId>en</ms:languageId>
		</ms:language>
		<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
		<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Json</ms:dataFormat>
		<ms:characterEncoding>http://w3id.org/meta-share/meta-share/UTF-8</ms:characterEncoding>
	</ms:outputResource>

----

.. _language:

language
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService.language``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Mandatory if applicable

:guilabel:`Explanation & Instructions`

Specifies the language that is used in the resource or supported by the tool/service, expressed according to the BCP47 recommendation

The element ``languageTag`` is composed of the ``languageId``, and optionally ``scriptId``, ``regionId`` and ``variantId``; you can use those elements that best describe the language(s) of your resource.

:guilabel:`Example`

.. code-block:: xml

	<ms:language>
		<ms:languageTag>en</ms:languageTag>
		<ms:languageId>en</ms:languageId>
	</ms:language>

	<ms:language>
		<ms:languageTag>en-US</ms:languageTag>
		<ms:languageId>en</ms:languageId>
		<ms:regionId>US</ms:regionId>
	</ms:language>

----

framework
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService.framework``

:guilabel:`Data type`	CV (`framework <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation//ELG-SHARE_xsd.html#framework>`_) 

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Specifies the implementation framework used for developing and running a tool/service

:guilabel:`Example`

.. code-block:: xml

	<ms:framework>http://w3id.org/meta-share/meta-share/TensorFlow</ms:framework>


----

implementationLanguage
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService.implementationLanguage``

:guilabel:`Data type`	string

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

The programming language(s) used for the development of a tool/service

:guilabel:`Example`

.. code-block:: xml

	<ms:implementationLanguage>Java v8.1</ms:implementationLanguage>


----

requiredHardware
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService.requiredHardware``

:guilabel:`Data type`	CV (`requiredHardware <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#requiredHardware>`_)

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Specifies the type of hardware required for running a tool and/or computational grammar

:guilabel:`Example`

.. code-block:: xml

	<ms:requiredHardware>http://w3id.org/meta-share/meta-share/ocrSystem</ms:requiredHardware>


----

trl
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService.trl``

:guilabel:`Data type`	CV (`TRL <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#trl>`_)

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Specifies the TRL (Technology Readiness Level) of the technology according to the measurement system defined by the EC (https://ec.europa.eu/research/participants/data/ref/h2020/wp/2014_2015/annexes/h2020-wp1415-annex-g-trl_en.pdf)

:guilabel:`Example`

.. code-block:: xml

	<ms:trl>http://w3id.org/meta-share/meta-share/trl4</ms:trl>

----

evaluated
================================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.ToolService.evaluated``

:guilabel:`Data type`	boolean

:guilabel:`Optionality`	Mandatory

:guilabel:`Explanation & Instructions`

Indicates whether the tool or service has been evaluated

If the tool/service has been evaluated, you can use the 'evaluation' component to give more detailed information; see `here <https://european-language-grid.readthedocs.io/en/release1.0.0/Documentation/ELG-SHARE_xsd.html#evaluation>`_ for the relevant elements.

:guilabel:`Example`

.. code-block:: xml

	<ms:evaluated>false</ms:evaluated>
