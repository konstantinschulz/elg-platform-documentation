.. _minimalProject:

Minimal elements for projects
#############################

This page describes the minimal metadata elements specific to **projects**.

----

Project
=======

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Project``	

:guilabel:`Data type`	component

:guilabel:`Optionality`	Mandatory

:guilabel:`Explanation & Instructions`

Wraps together elements for projects

:guilabel:`Example`

.. code-block:: xml

	<ms:Project>
		<ms:entityType>project</ms:entityType>
		...
	</ms:Project>

----

ProjectIdentifier
=================

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.ProjectIdentifier``

:guilabel:`Data type`   string

:guilabel:`Optionality` Recommended

:guilabel:`Explanations & Instructions`

A string (e.g., PID, internal to an organization, issued by the funding authority, etc.) used to uniquely identify a project

You must also use the attribute ``ProjectIdentifierScheme`` to specify the name of the scheme according to which an identifier is assigned to a project by the authority that issues it. `ProjectIdentifierScheme <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#ProjectIdentifierScheme>`_ for details.

:guilabel:`Example`

.. code-block:: xml

			<ms:ProjectIdentifier ms:ProjectIdentifierScheme="http://w3id.org/meta-share/meta-share/cordis">219608</ms:ProjectIdentifier>

			<ms:ProjectIdentifier ms:ProjectIdentifierScheme="http://w3id.org/meta-share/meta-share/cordis">219378</ms:ProjectIdentifier>
			
----

projectName
===========

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.projectName``

:guilabel:`Data type`   multilingual string

:guilabel:`Optionality` Mandatory

:guilabel:`Explanations & Instructions`

The full name (title) of a project

:guilabel:`Example`

.. code-block:: xml

			<ms:projectName xml:lang="en">Browser-based Multilingual Translation</ms:projectName>

			<ms:projectName xml:lang="en">European Language Grid</ms:projectName>

----

projectShortName
================

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.projectShortName``

:guilabel:`Data type`   multiligual string

:guilabel:`Optionality` Recommended

:guilabel:`Explanations & Instructions`

Introduces a short name (e.g., acronym, abbreviated form) by which a project is known

:guilabel:`Example`

.. code-block:: xml

			<ms:projectShortName xml:lang="en">Bergamot</ms:projectShortName>

			<ms:projectShortName xml:lang="en">ELG</ms:projectShortName>

----

projectAlternativeName
======================

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.projectAlternativeName``

:guilabel:`Data type`   multilingual string

:guilabel:`Optionality` Recommended

:guilabel:`Explanations & Instructions`

Introduces an alternative name (other than the short name) used for a project

:guilabel:`Example`

.. code-block:: xml

			<ms:projectAlternativeName xml:lang="en">The European Language Grid</ms:projectName>

----

fundingType
===========

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.fundingType``

:guilabel:`Data type`	CV (`fundingType <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#fundingType>`_)

:guilabel:`Optionality` Recommended

:guilabel:`Explanations & Instructions`

Specifies the type of funding of a project with regard to the source of the funding

:guilabel:`Example`

.. code-block:: xml

			<ms:fundingType>http://w3id.org/meta-share/meta-share/euFunds</ms:fundingType>

----

funder
======

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.funder``

:guilabel:`Data type`   component

:guilabel:`Optionality` Recommended

:guilabel:`Explanations & Instructions`

Identifies the person/organization/group that has financed the project

Funding information is important for acknowledgement purposes.

For organizations, you must provide the name of the organization (``organizationName``) and, if possible, a website (``website``) and/or an identifier (``OrganizationIdentifier``).

:guilabel:`Example`

.. code-block:: xml

			<ms:funder>
				<ms:Organization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">European Commission</ms:organizationName>
					<ms:website>https://ec.europa.eu/info/index_en</ms:website>
				</ms:Organization>
			</ms:funder>

----

fundingCountry
==============

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.fundingCountry``

:guilabel:`Data type`	CV (`regionIdType <https://european-language-grid.readthedocs.io/en/release1.0.0/Documentation/ISOVocabularies_xsd.html#fundingCountry>`_)

:guilabel:`Optionality` Recommended

:guilabel:`Explanations & Instructions`

Specifies the name of the funding country, in case of national funding as mentioned in ISO3166

:guilabel:`Example`

.. code-block:: xml

			<ms:fundingCountry>EU</ms:fundingCountry>

----

website
=======

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.website``

:guilabel:`Data type`   URL

:guilabel:`Optionality` Recommended

:guilabel:`Explanations & Instructions`

Links to a URL that acts as the primary page (like a table of contents) introducing information about an organization (e.g., products, contact information, etc.) or project

:guilabel:`Example`

.. code-block:: xml

			<ms:website>https://browser.mt/</ms:website>

			<ms:website>https://www.european-language-grid.eu/</ms:website>

----

logo
====

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.logo``

:guilabel:`Data type`   URL

:guilabel:`Optionality` Recommended
:guilabel:`Explanations & Instructions`

Links to a URL with an image file containing a symbol or graphic object used to identify the entity

:guilabel:`Example`

.. code-block:: xml

			<ms:logo>https://ufal.mff.cuni.cz/sites/default/files/styles/drupal_projects_logo_style/public/bergamot_logo.png</ms:logo>

			<ms:logo>https://www.european-language-grid.eu/wp-content/themes/elg_theme/fab/image/logo/rgb_elg__logo--colour.svg</ms:logo>

----

LTArea
======

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.LTArea``

:guilabel:`Data type`   component

:guilabel:`Optionality` Recommended

:guilabel:`Explanations & Instructions`

Introduces a Language Technology-related area that the project deals with

For details, see `LTArea <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#LTArea>`_
More specifically, you can fill in:

- the ``LTClassRecommended`` element with one of the recommended values from the `LT taxonomy <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#LTClassRecommended>`_, or
- the ``LTClassOther`` element with a free text.


:guilabel:`Example`

.. code-block:: xml

			<ms:LTArea>
				<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation</ms:LTClassRecommended>
			</ms:LTArea>
			<ms:LTArea>
				<ms:LTClassOther>Browser-based Machine Translation</ms:LTClassOther>
			</ms:LTArea>

----

domain
======

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.domain``

:guilabel:`Data type`   component

:guilabel:`Optionality` Recommended

:guilabel:`Explanations & Instructions`

Identifies a domain that the project deals with

You must fill in the ``CategoryLabel`` element with a free text value. If you prefer to add a value from an established controlled vocabulary, you can also use the ``DomainIdentifier`` (with the attribute ``DomainClassificationScheme`` with the appropriate value).

:guilabel:`Example`

.. code-block:: xml

			<ms:domain>
				<ms:categoryLabel xml:lang="en">htttp://w3id.org/meta-share/omtd-share/NewsMediaJournalismAndPublishing</ms:categoryLabel>
			</ms:domain>
			<ms:domain>
				<ms:categoryLabel xml:lang="en">General</ms:categoryLabel>
			</ms:domain>

----

keyword
=======

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.keyword``

:guilabel:`Data type`   multilingual string 

:guilabel:`Optionality` Recomended

:guilabel:`Explanations & Instructions`

Introduces a word or phrase considered important for the description of the project and thus used to index or classify it

:guilabel:`Example`

.. code-block:: xml

			<ms:keyword xml:lang="en">Machine translation</ms:keyword>
			<ms:keyword xml:lang="en">translation integration</ms:keyword>

			<ms:keyword xml:lang="en">Language technology services</ms:keyword>
			<ms:keyword xml:lang="en">Multilingualism</ms:keyword>
			<ms:keyword xml:lang="en">Less-resourced languages</ms:keyword>

----

socialMediaOccupationalAccount
==============================

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.socialMediaOccupationalAccount``

:guilabel:`Data type`   multilingual string

:guilabel:`Optionality` Recommended

:guilabel:`Explanations & Instructions`

Introduces the social media or occupational account details of a person, organization or project

You must also use the attribute ``socialMediaAccountType`` to specify the type of social media account. See  `socialMediaOccupationalAccountType <https://european-language-grid.readthedocs.io/en/release1.0.0/Documentation/ELG-SHARE_xsd.html#socialMediaOccupationalAccountType>`_ for details.

:guilabel:`Example`

.. note:: TODO: add example

projectSummary
==============

:guilabel:`Path`        ``MetadataRecord.DescribedEntity.Project.projectSummary``

:guilabel:`Data type`   multilingual string

:guilabel:`Optionality` Recommended

:guilabel:`Explanations & Instructions`

Introduces a short description (in free text) of the main objectives, mission or contents of the project

:guilabel:`Example`

.. code-block:: xml

			<ms:projectSummary xml:lang="en">'The Bergamot project will add and improve client-side machine translation in a web browser.  Unlike current cloud-based options, running directly on users'' machines empowers citizens to preserve their privacy and increases the uptake of language technologies in Europe in various sectors that require confidentiality.  Free software integrated with an open-source web browser, such as Mozilla Firefox, will enable bottom-up adoption by non-experts, resulting in cost savings for private and public sector users who would otherwise procure translation or operate monolingually.  To understand and support non-expert users, our user experience work package researches their needs and creates the user interface.  Rather than simply translating text, this interface will expose improved quality estimates, addressing the rising public debate on algorithmic trust.  Building on quality estimation research, we will enable users to confidently generate text in a language they do not speak, enabling cross-lingual online form filling.  To improve quality overall, dynamic domain adaptation research addresses the peculiar writing style of a website or user by adapting translation on the fly using local information too private to upload to the cloud.  These applications require adaptation and inference to run on desktop hardware with compact model downloads, which we address with neural network efficiency research.  Our combined research on user experience, domain adaptation, quality estimation, outbound translation, and efficiency support a broad browser-based innovation plan.'</ms:projectSummary>

			<ms:projectSummary xml:lang="en">With 24 official EU and many more additional languages, multilingualism in Europe and an inclusive Digital Single Market can only be enabled through Language Technologies (LTs). European LT business is dominated by thousands of SMEs and a few large players. Many are world-class, with technologies that outperform the global players. However, European LT business is also fragmented β€“ by nation states, languages, verticals and sectors. Likewise, while much of European LT research is world-class, with results transferred into industry and commercial products, its full impact is held back by fragmentation. The key issue and challenge is the fragmentation of the European LT landscape. The European Language Grid (ELG) project will address this fragmentation by establishing the ELG as the primary platform for LT in Europe. The ELG will be a scalable cloud platform, providing, in an easy-to-integrate way, access to hundreds of commercial and non-commercial Language Technologies for all European languages, including running tools and services as well as data sets and resources. It will enable the commercial and non-commercial European LT community to deposit and upload their technologies and data sets into the ELG, to deploy them through the grid, and to connect with other resources. The ELG will boost the Multilingual Digital Single Market towards a thriving European LT community, creating new jobs and opportunities. Through open calls, up to 20 pilot projects will be financially supported to demonstrate the usefulness of the ELG. The proposal is rooted in the experience of a consortium with partners involved in all relevant initiatives. Based on these, 30\\ national competence centres and the European LT Board will be set up for European coordination. The ELG will foster β€language technologies for Europe built in Europeβ€, tailored to our languages and cultures and to our societal and economical demands, benefitting the European citizen, society, innovation and industry.</ms:projectSummary>
