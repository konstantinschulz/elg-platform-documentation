.. _contributeModel:

Contribute a model
##################

This page describes how to contribute a :ref:`model<typesOfResources>` to the European Language Grid.

Before you start
----------------

- Please make sure that the model you want to contribute complies with our :ref:`terms of use <termsOfUse>`.
- Please make sure you have :ref:`registered<register>` and been assigned the :ref:`provider role<providerRole>`.
- For this release, you can provide the data files of the model

	- at a remote URL and include this information in the relevant metadata element (accessLocation), or 
	- if you want us to upload it at the ELG cloud area, contact us through the `ELG contact form <https://www.european-language-grid.eu/contact/>`_.


.. _metadataModel:

Step 1: create metadata
-----------------------

The first step is to describe your model using ELG’s metadata format, ELG-SHARE. Future releases of ELG will include an interactive editor for this. However, for now, you must create an XML file. Refer to the example below for how to do this.

The elements you need are documented on the following pages:

- :ref:`minimalAll`
- :ref:`minimalLRT`
- :ref:`minimalLangDesc`

For more information about ELG-SHARE, see:

- :ref:`basicSchema`

At the ELG GitLab, you will find `templates <https://gitlab.com/european-language-grid/platform/ELG-SHARE-schema/-/tree/master/metadata%20record%20templates>`_ (that you can use to create new metadata records) and `examples <https://gitlab.com/european-language-grid/platform/ELG-SHARE-schema/-/tree/master/metadata%20record%20examples>`_ in XML format.



Example: N-gram model
^^^^^^^^^^^^^^^^^^^^^
Published at https://live.european-language-grid.eu/catalogue/#/resource/service/ld/900

.. code-block:: xml

	<?xml version="1.0" encoding="UTF-8"?>
	<ms:MetadataRecord xmlns="http://w3id.org/meta-share/meta-share/" xmlns:datacite="http://purl.org/spar/datacite/" xmlns:dcat="http://www.w3.org/ns/dcat#" xmlns:ms="http://w3id.org/meta-share/meta-share/" xmlns:omtd="http://w3id.org/meta-share/omtd-share/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ ../../Schema/ELG-SHARE.xsd">
		<ms:MetadataRecordIdentifier ms:MetadataRecordIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">value automatically assigned - leave as is</ms:MetadataRecordIdentifier>
		<ms:metadataCreationDate>2020-10-03</ms:metadataCreationDate>
		<ms:metadataCurator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<ms:email>username@someDomain.com</ms:email>
		</ms:metadataCurator>
		<ms:compliesWith>http://w3id.org/meta-share/meta-share/ELG-SHARE</ms:compliesWith>
		<ms:metadataCreator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<ms:email>username@someDomain.com</ms:email>
		</ms:metadataCreator>
		<ms:DescribedEntity>
			<ms:LanguageResource>
				<ms:entityType>LanguageResource</ms:entityType>
				<ms:resourceName xml:lang="en">PANACEA Environment Corpus n-grams EL (Greek)</ms:resourceName>
				<ms:description xml:lang="en">PANACEA Environment Corpus n-grams EL (Greek) 1.0 contains Greek word n-grams and Greek word/tag/lemma n-grams in the "Environment" (ENV) domain. N-grams are accompanied by their observed frequency counts. The length of the n-grams ranges from unigrams (single words) to five-grams. The data were collected in the context of PANACEA (http://www.panacea-lr.eu), an EU-FP7 Funded Project under Grant Agreement 248064. 
	The n-gram counts were generated from crawled Web pages that were automatically detected to be in the Greek language and were automatically classified as relevant to the ENV domain. The collection consisted of approximately 31.71 million tokens. Data collection took place in the summer of 2011.</ms:description>
				<ms:version>v1.0</ms:version>
				<ms:additionalInfo>
					<ms:landingPage>http://nlp.ilsp.gr/panacea/D4.3/data/201209/gms/env_el/README.txt</ms:landingPage>
				</ms:additionalInfo>
				<ms:additionalInfo>
					<ms:email>contact@someDomain.com</ms:email>
				</ms:additionalInfo>
				<ms:contact>
					<ms:Person>
						<ms:actorType>Person</ms:actorType>
						<ms:surname xml:lang="en">Prokopidis</ms:surname>
						<ms:givenName xml:lang="en">Prokopis</ms:givenName>
						<ms:email>contact@someDomain.com</ms:email>
					</ms:Person>
				</ms:contact>
				<ms:contact>
					<ms:Person>
						<ms:actorType>Person</ms:actorType>
						<ms:surname xml:lang="en">Papavassiliou</ms:surname>
						<ms:givenName xml:lang="en">Vassilis</ms:givenName>
						<ms:email>contact@someDomain.com</ms:email>
					</ms:Person>
				</ms:contact>
				<ms:keyword xml:lang="en">corpus</ms:keyword>
				<ms:domain>
					<ms:categoryLabel xml:lang="en">environment</ms:categoryLabel>
				</ms:domain>
				<ms:resourceCreator>
					<ms:Organization>
						<ms:actorType>Organization</ms:actorType>
						<ms:organizationName xml:lang="en">Institute for Language and Speech Processing</ms:organizationName>
						<ms:website>http://www.ilsp.gr</ms:website>
					</ms:Organization>
				</ms:resourceCreator>
				<ms:creationStartDate>2011-06-01</ms:creationStartDate>
				<ms:creationEndDate>2011-08-31</ms:creationEndDate>
				<ms:fundingProject>
					<ms:projectName xml:lang="en">Platform for Automatic, Normalized Annotation and Cost-Effective Acquisition of Language Resources for Human Language </ms:projectName>
					<ms:website>http://www.panacea-lr.eu</ms:website>
				</ms:fundingProject>
				<ms:LRSubclass>
					<ms:LanguageDescription>
						<ms:lrType>LanguageDescription</ms:lrType>
						<ms:LanguageDescriptionSubclass>
							<ms:NGramModel>
								<ms:ldSubclassType>NGramModel</ms:ldSubclassType>
								<ms:baseItem>http://w3id.org/meta-share/meta-share/word</ms:baseItem>
								<ms:order>5</ms:order>
							</ms:NGramModel>
						</ms:LanguageDescriptionSubclass>
						<ms:LanguageDescriptionMediaPart>
							<ms:LanguageDescriptionTextPart>
								<ms:ldMediaType>LanguageDescriptionTextPart</ms:ldMediaType>
								<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
								<ms:lingualityType>http://w3id.org/meta-share/meta-share/monolingual</ms:lingualityType>
								<ms:language>
									<ms:languageTag>el</ms:languageTag>
									<ms:languageId>el</ms:languageId>
								</ms:language>
								<ms:metalanguage>
									<ms:languageTag>und</ms:languageTag>
									<ms:languageId>und</ms:languageId>
								</ms:metalanguage>
								<ms:creationDetails xml:lang="en">automatic web crawling, automatic language detection, data preprocessing (boilerpipe filtering, lemmatization &amp; tagging)</ms:creationDetails>
							</ms:LanguageDescriptionTextPart>
						</ms:LanguageDescriptionMediaPart>
						<ms:DatasetDistribution>
							<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
							<ms:accessLocation>http://metashare.ilsp.gr:8080/repository/download/490952dc1cec11e2b545842b2b6a04d78dc202de28d5421f91752610a781175e</ms:accessLocation>
							<ms:distributionTextFeature>
								<ms:size>
									<ms:amount>435189</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/unigram</ms:sizeUnit>
								</ms:size>
								<ms:size>
									<ms:amount>3.860716E6</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/bigram</ms:sizeUnit>
								</ms:size>
								<ms:size>
									<ms:amount>9.767383E6</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/trigram</ms:sizeUnit>
								</ms:size>
								<ms:size>
									<ms:amount>1.368394E7</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/four-gram</ms:sizeUnit>
								</ms:size>
								<ms:size>
									<ms:amount>1.495402E7</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/five-gram</ms:sizeUnit>
								</ms:size>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormat>
							</ms:distributionTextFeature>
							<ms:licenceTerms>
								<ms:licenceTermsName xml:lang="en">CC-BY-SA-4.0</ms:licenceTermsName>
								<ms:licenceTermsURL>https://spdx.org/licenses/CC-BY-SA-4.0.html</ms:licenceTermsURL>
							</ms:licenceTerms>
							<ms:attributionText xml:lang="en">This LR has been created by Athena R.C./ILSP (www.ilsp.gr) and is licensed under a CC-BY-SA licence</ms:attributionText>
						</ms:DatasetDistribution>
						<ms:personalDataIncluded>false</ms:personalDataIncluded>
						<ms:sensitiveDataIncluded>false</ms:sensitiveDataIncluded>
					</ms:LanguageDescription>
				</ms:LRSubclass>
			</ms:LanguageResource>
		</ms:DescribedEntity>
	</ms:MetadataRecord>

.. _uploadModel:

Step 2: upload
--------------

From the ELG catalogue, click the “Upload” link as shown below:

.. figure:: Upload01.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/
   :alt: Upload menu

Now upload the file you created in Step 1:

.. figure:: Upload02.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/
   :alt: Upload metadata XML

If there are any errors in your XML file, these will be shown to you. Fix them and try the upload again. Eventually, a success message will be shown to you and the metadata will be imported into the database.

Step 3: wait for approval
-------------------------

At this stage, the metadata record is only visible to you and to us, the ELG platform administrators. We will check your contribution and integrate it into the ELG catalogue if everything is in order, and contact you otherwise.