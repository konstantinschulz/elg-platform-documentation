.. _updateResource:

Update / Delete a catalogue entry
##################################

.. note:: Instructions on how to update or delete entries in the ELG catalogue will be provided shortly.