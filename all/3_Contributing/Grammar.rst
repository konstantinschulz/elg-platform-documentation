.. _contributeGrammar:

Contribute a grammar
####################

This page describes how to contribute a :ref:`grammar<typesOfResources>` to the European Language Grid.

Before you start
----------------

- Please make sure that the grammar you want to contribute complies with our :ref:`terms of use <termsOfUse>`.
- Please make sure you have :ref:`registered<register>` and been assigned the :ref:`provider role<providerRole>`.
- For this release, you can provide the data files of the grammar

	- at a remote URL and include this information in the relevant metadata element (accessLocation), or 
	- if you want us to upload it at the ELG cloud area, contact us through the `ELG contact form <https://www.european-language-grid.eu/contact/>`_.


.. _metadataGrammar:

Step 1: create metadata
-----------------------

The first step is to describe your grammar using ELG’s metadata format, ELG-SHARE. Future releases of ELG will include an interactive editor for this. However, for now, you must create an XML file. Refer to the examples below for how to do this.

The elements you need are documented on the following pages:

- :ref:`minimalAll`
- :ref:`minimalLRT`
- :ref:`minimalLangDesc`

For more information about ELG-SHARE, see:

- :ref:`basicSchema`

At the ELG GitLab, you will find `templates <https://gitlab.com/european-language-grid/platform/ELG-SHARE-schema/-/tree/master/metadata%20record%20templates>`_ (that you can use to create new metadata records) and `examples <https://gitlab.com/european-language-grid/platform/ELG-SHARE-schema/-/tree/master/metadata%20record%20examples>`_ in XML format.



Example: Monolingual computational grammar for a specific domain
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tourism Italian grammar

Published at: https://live.european-language-grid.eu/catalogue/#/resource/service/ld/901

.. code-block:: xml
 
	<?xml version="1.0" encoding="UTF-8"?>
	<ms:MetadataRecord xmlns="http://w3id.org/meta-share/meta-share/" xmlns:datacite="http://purl.org/spar/datacite/" xmlns:dcat="http://www.w3.org/ns/dcat#" xmlns:ms="http://w3id.org/meta-share/meta-share/" xmlns:omtd="http://w3id.org/meta-share/omtd-share/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ ../../Schema/ELG-SHARE.xsd">
		<ms:MetadataRecordIdentifier ms:MetadataRecordIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">value automatically assigned - leave as is</ms:MetadataRecordIdentifier>
		<ms:metadataCreationDate>2020-10-03</ms:metadataCreationDate>
		<ms:metadataCurator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<ms:email>username@someDomain.com</ms:email>
		</ms:metadataCurator>
		<ms:compliesWith>http://w3id.org/meta-share/meta-share/ELG-SHARE</ms:compliesWith>
		<ms:metadataCreator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<ms:email>username@someDomain.com</ms:email>
		</ms:metadataCreator>
		<ms:DescribedEntity>
			<ms:LanguageResource>
				<ms:entityType>LanguageResource</ms:entityType>
				<ms:resourceName xml:lang="en">Tourism Italian grammar</ms:resourceName>
				<ms:resourceShortName xml:lang="en">Tour.ita.grm</ms:resourceShortName>
				<ms:description xml:lang="en">Tourism Italian abnf grammar, manually created. Created within the Portdial project</ms:description>
				<ms:version>v1.0.0 (automatically assigned)</ms:version>
				<ms:additionalInfo>
					<ms:landingPage>https://sites.google.com/site/portdial2</ms:landingPage>
				</ms:additionalInfo>
				<ms:additionalInfo>
					<ms:email>contact@someDomain.com</ms:email>
				</ms:additionalInfo>
				<ms:contact>
					<ms:Person>
						<ms:actorType>Person</ms:actorType>
						<ms:surname xml:lang="en">Potamianos</ms:surname>
						<ms:givenName xml:lang="en">Alex</ms:givenName>
						<ms:email>contact@someDomain.com</ms:email>
					</ms:Person>
				</ms:contact>
				<ms:keyword xml:lang="en">languagedescription</ms:keyword>
				<ms:fundingProject>
					<ms:projectName xml:lang="en">Portdial</ms:projectName>
				</ms:fundingProject>
				<ms:LRSubclass>
					<ms:LanguageDescription>
						<ms:lrType>LanguageDescription</ms:lrType>
						<ms:LanguageDescriptionSubclass>
							<ms:Grammar>
								<ms:ldSubclassType>Grammar</ms:ldSubclassType>
								<ms:encodingLevel>http://w3id.org/meta-share/meta-share/morphology</ms:encodingLevel>
							</ms:Grammar>
						</ms:LanguageDescriptionSubclass>
						<ms:LanguageDescriptionMediaPart>
							<ms:LanguageDescriptionTextPart>
								<ms:ldMediaType>LanguageDescriptionTextPart</ms:ldMediaType>
								<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
								<ms:lingualityType>http://w3id.org/meta-share/meta-share/monolingual</ms:lingualityType>
								<ms:language>
									<ms:languageTag>it</ms:languageTag>
									<ms:languageId>it</ms:languageId>
								</ms:language>
								<ms:metalanguage>
									<ms:languageTag>und</ms:languageTag>
									<ms:languageId>und</ms:languageId>
								</ms:metalanguage>
							</ms:LanguageDescriptionTextPart>
						</ms:LanguageDescriptionMediaPart>
						<ms:DatasetDistribution>
							<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
							<ms:accessLocation>http://accessURL</ms:accessLocation>
							<ms:licenceTerms>
								<ms:licenceTermsName xml:lang="en">CC-BY-SA-4.0</ms:licenceTermsName>
								<ms:licenceTermsURL>https://spdx.org/licenses/CC-BY-SA-4.0.html</ms:licenceTermsURL>
							</ms:licenceTerms>
						</ms:DatasetDistribution>
						<ms:personalDataIncluded>false</ms:personalDataIncluded>
						<ms:sensitiveDataIncluded>false</ms:sensitiveDataIncluded>
					</ms:LanguageDescription>
				</ms:LRSubclass>
			</ms:LanguageResource>
		</ms:DescribedEntity>
	</ms:MetadataRecord>

.. _uploadGrammar:

Step 2: upload
--------------

From the ELG catalogue, click the “Upload” link as shown below:

.. figure:: Upload01.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/
   :alt: Upload menu

Now upload the file you created in Step 1:

.. figure:: Upload02.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/
   :alt: Upload metadata XML

If there are any errors in your XML file, these will be shown to you. Fix them and try the upload again. Eventually, a success message will be shown to you and the metadata will be imported into the database.

Step 3: wait for approval
-------------------------

At this stage, the metadata record is only visible to you and to us, the ELG platform administrators. We will check your contribution and integrate it into the ELG catalogue if everything is in order, and contact you otherwise.