.. _contributeOrganisation:

Contribute an organisation
##########################

This page describes how to contribute information on an :ref:`organisation<typesOfResources>` to the European Language Grid.

Before you start
----------------

- Please make sure that the metadata record you want to contribute complies with our :ref:`terms of use <termsOfUse>`.
- Please make sure you have :ref:`registered<register>` and been assigned the :ref:`provider role<providerRole>`.

Step 1: create metadata
-----------------------

The first step is to describe your organisation using ELG’s metadata format, ELG-SHARE. Future releases of ELG will include an interactive editor for this. However, for now, you must create an XML file. Refer to the examples below for how to do this.

The elements you need are documented on the following pages:

- :ref:`minimalAll`
- :ref:`minimalOrganisation`

For more information about ELG-SHARE, see:

- :ref:`basicSchema`

At the ELG GitLab, you will find `templates <https://gitlab.com/european-language-grid/platform/ELG-SHARE-schema/-/tree/master/metadata%20record%20templates>`_ (that you can use to create new metadata records) and `examples <https://gitlab.com/european-language-grid/platform/ELG-SHARE-schema/-/tree/master/metadata%20record%20examples>`_ in XML format.



Example 1: University
^^^^^^^^^^^^^^^^^^^^^

Charles University, Prague

Published at: https://live.european-language-grid.eu/catalogue/#/resource/organizations/385

.. code-block:: xml

	<?xml version="1.0" encoding="UTF-8"?>
	<ms:MetadataRecord xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ ../../Schema/ELG-SHARE.xsd" xmlns:ms="http://w3id.org/meta-share/meta-share/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<ms:MetadataRecordIdentifier ms:MetadataRecordIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">value automatically assigned - leave as is</ms:MetadataRecordIdentifier>
		<ms:metadataCreationDate>2020-01-07</ms:metadataCreationDate>
		<ms:metadataLastDateUpdated>2020-01-07</ms:metadataLastDateUpdated>
		<ms:metadataCurator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<ms:email>smith@example.com</ms:email>
		</ms:metadataCurator>
		<ms:compliesWith>http://w3id.org/meta-share/meta-share/ELG-SHARE</ms:compliesWith>
		<ms:metadataCreator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<ms:email>smith@example.com</ms:email>
		</ms:metadataCreator>
		<ms:DescribedEntity>
		<ms:Organization>
		<ms:entityType>Organization</ms:entityType>
		<ms:OrganizationIdentifier ms:OrganizationIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">automatically assigned by ELG - please don't change</ms:OrganizationIdentifier>
		<ms:organizationName xml:lang="en">Charles University</ms:organizationName>
		<ms:organizationShortName xml:lang="en">CUNI</ms:organizationShortName>
		<ms:organizationAlternativeName xml:lang="en">UNIVERZITA KARLOVA</ms:organizationAlternativeName>
		<ms:organizationRole>http://w3id.org/meta-share/meta-share/LTSupplier</ms:organizationRole>
		<ms:organizationRole>http://w3id.org/meta-share/meta-share/researchOrganization</ms:organizationRole>
		<ms:organizationRole>http://w3id.org/meta-share/meta-share/languageServiceProvider</ms:organizationRole>
		<ms:organizationLegalStatus>http://w3id.org/meta-share/meta-share/academicInstitution</ms:organizationLegalStatus>
		<ms:organizationBio xml:lang="en">Charles University was founded in 1348, making it one of the oldest universities in the world. Yet it is also renowned as a modern, dynamic, cosmopolitan and prestigious institution of higher education. It is the largest and most renowned Czech university, and is also the best-rated Czech university according to international rankings. There are currently 17 faculties at the University (14 in Prague, 2 in Hradec KrΓ΅lovΓ© and 1 in PlzeΕ), plus 3 institutes, 6 other centres of teaching, research, development and other creative activities, a centre providing information services, 5 facilities serving the whole University, and the Rectorate - which is the executive management body for the whole University.</ms:organizationBio>
		<ms:logo>https://cuni.cz/UKEN-1-version1-afoto.jpg</ms:logo>
		<ms:LTArea>
			<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LanguageTechnology</ms:LTClassRecommended>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation</ms:LTClassRecommended>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/SpeechRecognition</ms:LTClassRecommended>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/Annotation</ms:LTClassRecommended>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LexiconCreation</ms:LTClassRecommended>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassOther>language resources creation</ms:LTClassOther>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassOther>Dialog systems</ms:LTClassOther>
		</ms:LTArea>
		<ms:keyword xml:lang="en">Computational Linguistics</ms:keyword>
		<ms:keyword xml:lang="en">Natural Language Processing</ms:keyword>
		<ms:keyword xml:lang="en">Language Resources</ms:keyword>
		<ms:keyword xml:lang="en">Research infrastructures</ms:keyword>
		<ms:keyword xml:lang="en">Language Resources</ms:keyword>
		<ms:keyword xml:lang="en">Digital Humanities</ms:keyword>
		<ms:website>https://www.cuni.cz</ms:website>
		<ms:headOfficeAddress>
			<ms:address xml:lang="en">OVOCNY TRH 560/5</ms:address>
			<ms:zipCode>116 36</ms:zipCode>
			<ms:city xml:lang="en">PRAHA 1</ms:city>
			<ms:country>CZ</ms:country>
		</ms:headOfficeAddress>
		<ms:hasDivision>
			<ms:divisionName xml:lang="en">Institute of Formal and Applied Linguistics</ms:divisionName>
			<ms:divisionShortName xml:lang="en">UFAL</ms:divisionShortName>
			<ms:divisionCategory>http://w3id.org/meta-share/meta-share/institute</ms:divisionCategory>
			<ms:organizationBio xml:lang="en">'Institute of Formal and Applied Linguistics (ΓFAL) at the Computer Science School, Faculty of Mathematics and Physics, Charles University, Czech Republic.  The institute was established in 1990 as a continuation of the research  and teaching activities carried out by the former Laboratory of Algebraic Linguistics since the early 60s at the Faculty of Philosophy and later at the Faculty of Mathematics and Physics, Charles University. The Institute is a primarily research department working on many topics in the area of Computational Linguistics, and on many research projects both nationally and internationally. However, the Institute of Formal and Applied Linguistics is also a regular department in the sense that it carries a comprehensive teaching program both for the Master''s degree (Mgr., or MSc.) as well as for a doctorate (Ph.D.) in Computational Linguistics. Both programs are taught in Czech and English. The Institute is also a member of the double- degree \"Master''s LCT programme\" of the EU. Students also can take advantage of the Erasmus program for typically semester-long stays at partner Universities abroad. '</ms:organizationBio>
			<ms:logo>https://ufal.mff.cuni.cz/sites/all/themes/drufal/css/logo/logo_ufal_110u.png</ms:logo>
		<ms:LTArea>
			<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation</ms:LTClassRecommended>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/SpeechRecognition</ms:LTClassRecommended>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/Annotation</ms:LTClassRecommended>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LexiconCreation</ms:LTClassRecommended>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassOther>Lexical Resources</ms:LTClassOther>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassOther>Dialog systems</ms:LTClassOther>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassOther>Corpus Creation</ms:LTClassOther>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassOther>Research Infrastructure</ms:LTClassOther>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassOther>LT services</ms:LTClassOther>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassOther>NLP Support</ms:LTClassOther>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassOther>Digital Humanities</ms:LTClassOther>
		</ms:LTArea>
		<ms:keyword xml:lang="en">Computational Linguistics</ms:keyword>
			<ms:headOfficeAddress>
				<ms:address xml:lang="en">MalostranskΓ© nΓ΅m. 25</ms:address>
				<ms:zipCode>11800</ms:zipCode>
				<ms:city xml:lang="en">Praha 1</ms:city>
				<ms:country>CZ</ms:country>
			</ms:headOfficeAddress>
		</ms:hasDivision>
		</ms:Organization>
	</ms:DescribedEntity>
	</ms:MetadataRecord>
	
	
Example 2: SME
^^^^^^^^^^^^^^

**SME**: Evaluation and Language Resources Distribution Agency (ELDA)

Published at: https://live.european-language-grid.eu/catalogue/#/resource/organizations/646
   
.. code-block:: xml

	<?xml version="1.0" encoding="UTF-8"?>
	<ms:MetadataRecord xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ ../../Schema/ELG-SHARE.xsd" xmlns:ms="http://w3id.org/meta-share/meta-share/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<ms:MetadataRecordIdentifier ms:MetadataRecordIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">value automatically assigned - leave as is</ms:MetadataRecordIdentifier>
		<ms:metadataCreationDate>2020-01-07</ms:metadataCreationDate>
		<ms:metadataLastDateUpdated>2020-01-07</ms:metadataLastDateUpdated>
		<ms:metadataCurator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<ms:email>smith@example.com</ms:email>
		</ms:metadataCurator>
		<ms:compliesWith>http://w3id.org/meta-share/meta-share/ELG-SHARE</ms:compliesWith>
		<ms:metadataCreator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<ms:email>smith@example.com</ms:email>
		</ms:metadataCreator>
		<ms:DescribedEntity>
		<ms:Organization>
		<ms:entityType>Organization</ms:entityType>
		<ms:OrganizationIdentifier ms:OrganizationIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">automatically assigned by ELG - please don't change</ms:OrganizationIdentifier>
		<ms:organizationName xml:lang="en">Evaluation and Language Resources Distribution Agency</ms:organizationName>
		<ms:organizationShortName xml:lang="en">ELDA</ms:organizationShortName>
		<ms:organizationAlternativeName xml:lang="en">EVALUATIONS AND LANGUAGE RESOURCES DISTRIBUTION AGENCY</ms:organizationAlternativeName>
		<ms:organizationRole>http://w3id.org/meta-share/meta-share/LTSupplier</ms:organizationRole>
		<ms:organizationRole>http://w3id.org/meta-share/meta-share/dataEvaluator</ms:organizationRole>
		<ms:organizationLegalStatus>http://w3id.org/meta-share/meta-share/sme</ms:organizationLegalStatus>
		<ms:organizationBio xml:lang="en">The Evaluations and Language Resources Distribution Agency (ELDA), was created in 1995 as the organizational infrastructure with the mission of  providing a central clearing house for Language Resources (LR) of the  European Language Resources Association (ELRA). ELDA was set up to  identify, classify, collect, validate and distribute the language resources that are  needed by the Human Language Technology (HLT) community. Anticipating the   evolutions in the HLT field, ELDA broadened its activities to cover  multimedia/multimodal resources as well as evaluation activities, distributing  the language resources needed for evaluation purposes, and conducting/coordinating evaluation campaigns. ELDA has played a significant role within the major Multimedia and Multimodal production projects that resulted in one of the most impressive catalogues of available data sets, embracing all aspects of Language Technologies. ELDA was also involved in evaluation initiatives, in several FPs’ projects involving HLT infrastructures, as well as in national programmes. In addition to work on data production, processing and annotation, validation and quality control, several of these projects also involved work on legal framework management for the produced resources. Moreover, ELDA has contributed to the development of open platforms and has joined forces with other European key players by bringing its assets (LR catalogue, evaluation services and benchmarking) to constitute Europe's backbone for Language Resources sharing and distribution. ELDA is also the initiator of the Language Resource and the Evaluation Conference (LREC), since 1998. With over 1200 participants, LREC is the major event on Language Resources (LRs) and Evaluation for Human Language Technologies (HLT).</ms:organizationBio>
		<ms:logo>https://www.european-language-grid.eu/wp-content/uploads/2019/03/logo__consortium-elda.svg</ms:logo>
		<ms:LTArea>
			<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LanguageTechnology</ms:LTClassRecommended>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/Evaluation</ms:LTClassRecommended>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassOther>Language Resource collection, processing, production</ms:LTClassOther>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassOther>legal clearing</ms:LTClassOther>
		</ms:LTArea>
		<ms:LTArea>
			<ms:LTClassOther>HLT evaluation and dissemination</ms:LTClassOther>
		</ms:LTArea>
		<ms:keyword xml:lang="en">Language Resources and Evaluation</ms:keyword>
		<ms:keyword xml:lang="en">Legal support</ms:keyword>
		<ms:keyword xml:lang="en">Data management</ms:keyword>
		<ms:website>http://www.elra.info/en/</ms:website>
		<ms:headOfficeAddress>
			<ms:address xml:lang="en">9 RUE DES CORDELIERES</ms:address>
			<ms:zipCode>75 013</ms:zipCode>
			<ms:city xml:lang="en">Paris</ms:city>
			<ms:country>FR</ms:country>
		</ms:headOfficeAddress>
	</ms:Organization>
	</ms:DescribedEntity>
	</ms:MetadataRecord>


Step 2: upload
--------------

From the ELG catalogue, click the “Upload” link as shown below:

.. figure:: Upload01.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/
   :alt: Upload menu

Now upload the file you created in Step 1:

.. figure:: Upload02.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/
   :alt: Upload metadata XML

If there are any errors in your XML file, these will be shown to you. Fix them and try the upload again. Eventually, a success message will be shown to you and the metadata will be imported into the database.

Step 3: wait for approval
-------------------------

At this stage, the metadata record is only visible to you and to us, the ELG platform administrators. We will check your contribution and integrate it into the ELG catalogue if everything is in order, and contact you otherwise.