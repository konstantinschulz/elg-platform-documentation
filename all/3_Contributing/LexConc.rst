.. _contributeLexConc:

Contribute a lexical/conceptual resource
########################################

This page describes how to contribute a :ref:`lexical/conceptual resource<typesOfResources>` to the European Language Grid.

Before you start
----------------

- Please make sure that the lexical/conceptual resources you want to contribute complies with our :ref:`terms of use <termsOfUse>`.
- Please make sure you have :ref:`registered<register>` and been assigned the :ref:`provider role<providerRole>`.
- For this release, you can provide the data files of the lexical/conceptual resource

	- at a remote URL and include this information in the relevant metadata element (accessLocation), or 
	- if you want us to upload it at the ELG cloud area, contact us through the `ELG contact form <https://www.european-language-grid.eu/contact/>`_.



.. _metadataLexConc:

Step 1: create metadata
-----------------------

The first step is to describe your lexical/conceptual resource using ELG’s metadata format, ELG-SHARE. Future releases of ELG will include an interactive editor for this. However, for now, you must create an XML file. Refer to the examples below for how to do this.

The elements you need are documented on the following pages:

- :ref:`minimalAll`
- :ref:`minimalLRT`
- :ref:`minimalLexConc`

For more information about ELG-SHARE, see:

- :ref:`basicSchema`

At the ELG GitLab, you will find `templates <https://gitlab.com/european-language-grid/platform/ELG-SHARE-schema/-/tree/master/metadata%20record%20templates>`_ (that you can use to create new metadata records) and `examples <https://gitlab.com/european-language-grid/platform/ELG-SHARE-schema/-/tree/master/metadata%20record%20examples>`_ in XML format.



Example 1: Terminological lexicon
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

INTERA Corpus - the Bulgarian-English terms from the BG-EN pair

Published at: https://live.european-language-grid.eu/catalogue/#/resource/service/lcr/694

.. code-block:: xml

	<?xml version="1.0" encoding="UTF-8"?>
	<ms:MetadataRecord xmlns="http://w3id.org/meta-share/meta-share/" xmlns:datacite="http://purl.org/spar/datacite/" xmlns:dcat="http://www.w3.org/ns/dcat#" xmlns:ms="http://w3id.org/meta-share/meta-share/" xmlns:omtd="http://w3id.org/meta-share/omtd-share/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ ../../Schema/ELG-SHARE.xsd">
		<ms:MetadataRecordIdentifier ms:MetadataRecordIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">value automatically assigned - leave as is</ms:MetadataRecordIdentifier>
		<ms:metadataCreationDate>2020-02-02</ms:metadataCreationDate>
		<ms:metadataCurator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<ms:email>curator@somedomain.com</ms:email>
		</ms:metadataCurator>
		<ms:compliesWith>http://w3id.org/meta-share/meta-share/ELG-SHARE</ms:compliesWith>
		<ms:metadataCreator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<ms:email>curator@somedomain.com</ms:email>
		</ms:metadataCreator>
		<ms:DescribedEntity>
			<ms:LanguageResource>
				<ms:entityType>LanguageResource</ms:entityType>
				<ms:resourceName xml:lang="en">INTERA Corpus - the Bulgarian-English terms from the BG-EN pair</ms:resourceName>
				<ms:description xml:lang="en">The Bulgarian-English terms from the BG-EN pair of the INTERA corpus; written language, domain specific (law, education).</ms:description>
				<ms:version>v1.0.0 (automatically assigned)</ms:version>
				<ms:additionalInfo>
					<ms:email>contact@somedomain.com</ms:email>
				</ms:additionalInfo>
				<ms:contact>
					<ms:Person>
						<ms:actorType>Person</ms:actorType>
						<ms:surname xml:lang="en">Gavrilidou</ms:surname>
						<ms:givenName xml:lang="en">Maria</ms:givenName>
						<ms:email>contact@somedomain.com</ms:email>
					</ms:Person>
				</ms:contact>
				<ms:keyword xml:lang="en">lexicalconceptualresource</ms:keyword>
				<ms:domain>
					<ms:categoryLabel xml:lang="en">education</ms:categoryLabel>
				</ms:domain>
				<ms:domain>
					<ms:categoryLabel xml:lang="en">law</ms:categoryLabel>
				</ms:domain>
				<ms:creationStartDate>2003-01-01</ms:creationStartDate>
				<ms:creationEndDate>2004-12-31</ms:creationEndDate>
				<ms:fundingProject>
					<ms:projectName xml:lang="en">Integrated European language data Repository Area</ms:projectName>
					<ms:website>http://www.elda.org/intera</ms:website>
				</ms:fundingProject>
				<ms:intendedApplication>
					<ms:LTClassOther>machineTranslation</ms:LTClassOther>
				</ms:intendedApplication>
				<ms:actualUse>
					<ms:usedInApplication>
						<ms:LTClassOther>terminologyExtraction</ms:LTClassOther>
					</ms:usedInApplication>
					<ms:actualUseDetails xml:lang="en">nlpApplications</ms:actualUseDetails>
				</ms:actualUse>
				<ms:isDocumentedBy>
					<ms:title xml:lang="en">Building Multilingual Terminological Resources</ms:title>
				</ms:isDocumentedBy>
				<ms:isDocumentedBy>
					<ms:title xml:lang="en">Building parallel corpora for eContent professionals</ms:title>
				</ms:isDocumentedBy>
				<ms:isDocumentedBy>
					<ms:title xml:lang="en">Language resources production models: the case of INTERA multilingual corpus and terminology</ms:title>
				</ms:isDocumentedBy>
				<ms:isDocumentedBy>
					<ms:title xml:lang="en">D5.2 - Report on the multilingual resources production</ms:title>
					<ms:DocumentIdentifier ms:DocumentIdentifierScheme="http://purl.org/spar/datacite/url">http://www.elda.org/article176.html</ms:DocumentIdentifier>
				</ms:isDocumentedBy>
				<ms:relation>
					<ms:relationType xml:lang="en">isExtractedfrom</ms:relationType>
					<ms:relatedLR>
						<ms:resourceName xml:lang="en">INTERA corpus</ms:resourceName>
					</ms:relatedLR>
				</ms:relation>
				<ms:LRSubclass>
					<ms:LexicalConceptualResource>
						<ms:lrType>LexicalConceptualResource</ms:lrType>
						<ms:lcrSubclass>http://w3id.org/meta-share/meta-share/wordlist</ms:lcrSubclass>
						<ms:encodingLevel>http://w3id.org/meta-share/meta-share/morphology</ms:encodingLevel>
						<ms:LexicalConceptualResourceMediaPart>
							<ms:LexicalConceptualResourceTextPart>
								<ms:lcrMediaType>LexicalConceptualResourceTextPart</ms:lcrMediaType>
								<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
								<ms:lingualityType>http://w3id.org/meta-share/meta-share/bilingual</ms:lingualityType>
								<ms:multilingualityType>http://w3id.org/meta-share/meta-share/parallel</ms:multilingualityType>
								<ms:language>
									<ms:languageTag>bg</ms:languageTag>
									<ms:languageId>bg</ms:languageId>
								</ms:language>
								<ms:language>
									<ms:languageTag>en</ms:languageTag>
									<ms:languageId>en</ms:languageId>
								</ms:language>
								<ms:metalanguage>
									<ms:languageTag>und</ms:languageTag>
									<ms:languageId>und</ms:languageId>
								</ms:metalanguage>
								<ms:modalityType>http://w3id.org/meta-share/meta-share/writtenLanguage</ms:modalityType>
							</ms:LexicalConceptualResourceTextPart>
						</ms:LexicalConceptualResourceMediaPart>
						<ms:DatasetDistribution>
							<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
							<ms:accessLocation>http://metashare.ilsp.gr:8080/repository/download/cdba66329e8111e581e1842b2b6a04d770c91fa84de04f259240aa450aaa9081</ms:accessLocation>
							<ms:distributionTextFeature>
								<ms:size>
									<ms:amount>7581</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/word3</ms:sizeUnit>
								</ms:size>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormat>
							</ms:distributionTextFeature>
							<ms:licenceTerms>
								<ms:licenceTermsName xml:lang="en">CC-BY-4.0</ms:licenceTermsName>
								<ms:licenceTermsURL>https://spdx.org/licenses/CC-BY-4.0.html</ms:licenceTermsURL>
								<ms:LicenceIdentifier ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">ELG-ENT-LIC-270220-00000072</ms:LicenceIdentifier>
							</ms:licenceTerms>
							<ms:attributionText xml:lang="en">The INTERA Corpus - the Bulgarian-English terms from the BG-EN pair of the ILSP/RC Athena licensed under CC-BY as accessed via META-SHARE</ms:attributionText>
						</ms:DatasetDistribution>
						<ms:personalDataIncluded>false</ms:personalDataIncluded>
						<ms:sensitiveDataIncluded>false</ms:sensitiveDataIncluded>
					</ms:LexicalConceptualResource>
				</ms:LRSubclass>
			</ms:LanguageResource>
		</ms:DescribedEntity>
	</ms:MetadataRecord>

Example 2: Computational lexicon
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

MCL - Multifunctional Computational Lexicon of Contemporary Portuguese

Published at: https://live.european-language-grid.eu/catalogue/#/resource/service/lcr/918

.. code-block:: xml
   
	<?xml version="1.0" encoding="UTF-8"?>
	<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ ../../Schema/ELG-SHARE.xsd">
		<ms:MetadataRecordIdentifier ms:MetadataRecordIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">value automatically assigned - leave as is</ms:MetadataRecordIdentifier>
		<ms:metadataCreationDate>2005-05-12</ms:metadataCreationDate>
		<ms:metadataLastDateUpdated>2020-02-24</ms:metadataLastDateUpdated>
		<ms:metadataCurator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<ms:email>curator@somedomain.com</ms:email>
		</ms:metadataCurator>
		<ms:compliesWith>http://w3id.org/meta-share/meta-share/ELG-SHARE</ms:compliesWith>
		<ms:metadataCreator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<ms:email>curator@somedomain.com</ms:email>
		</ms:metadataCreator>
		<ms:DescribedEntity>
			<ms:LanguageResource>
				<ms:entityType>LanguageResource</ms:entityType>
				<ms:resourceName xml:lang="en">MCL - Multifunctional Computational Lexicon of Contemporary Portuguese</ms:resourceName>
				<ms:description xml:lang="en">MCL is a 26,443 lemma Frequency Lexicon with 140,315 tokens, with the minimum lemma frequency of 6, extracted from CORLEX, a contemporary Portuguese corpus (16,210,438 words). CORLEX is a subcorpus of the Reference Corpus of Contemporary Portuguese and contains written and spoken texts of several types, being genre diversity a characteristic of this corpus. CORLEX contains mainly journalistic texts (56% of the written corpus and 53% of the whole corpus). In order to extract the lexicon, all the different lexical forms occurring in the corpus were indexed and subsequently tagged morphosyntactically and lemmatised by PALAVROSO. Each lemma in MCL is followed by morphosyntactic and quantitative information. The same information is given regarding each lemma token (inflected forms and some compounds). The lexicon indexations are listed in alphabetical order or decreasing frequency order.</ms:description>
				<ms:LRIdentifier ms:LRIdentifierScheme="http://w3id.org/meta-share/meta-share/islrn">489-956-642-755-8</ms:LRIdentifier>
				<ms:LRIdentifier ms:LRIdentifierScheme="http://w3id.org/meta-share/meta-share/other">ELRA-L0096</ms:LRIdentifier>
				<ms:version>1.0</ms:version>
				<ms:versionDate>2016-01-20</ms:versionDate>
				<ms:additionalInfo>
					<ms:landingPage>http://catalog.elra.info/product_info.php?products_id=1254</ms:landingPage>
				</ms:additionalInfo>
				<ms:additionalInfo>
					<ms:email>contact@somedomain.com</ms:email>
				</ms:additionalInfo>
				<ms:keyword xml:lang="en">lexicalconceptualresource</ms:keyword>
				<ms:LRSubclass>
					<ms:LexicalConceptualResource>
						<ms:lrType>LexicalConceptualResource</ms:lrType>
						<ms:lcrSubclass>http://w3id.org/meta-share/meta-share/lexicon</ms:lcrSubclass>
						<ms:encodingLevel>http://w3id.org/meta-share/meta-share/morphology</ms:encodingLevel>
						<ms:encodingLevel>http://w3id.org/meta-share/meta-share/syntax</ms:encodingLevel>
						<ms:LexicalConceptualResourceMediaPart>
							<ms:LexicalConceptualResourceTextPart>
								<ms:lcrMediaType>LexicalConceptualResourceTextPart</ms:lcrMediaType>
								<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
								<ms:lingualityType>http://w3id.org/meta-share/meta-share/monolingual</ms:lingualityType>
								<ms:language>
									<ms:languageTag>pt</ms:languageTag>
									<ms:languageId>pt</ms:languageId>
								</ms:language>
								<ms:metalanguage>
									<ms:languageTag>und</ms:languageTag>
									<ms:languageId>und</ms:languageId>
								</ms:metalanguage>
							</ms:LexicalConceptualResourceTextPart>
						</ms:LexicalConceptualResourceMediaPart>
						<ms:DatasetDistribution>
							<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
							<ms:distributionTextFeature>
								<ms:size>
									<ms:amount>26443</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/entry</ms:sizeUnit>
								</ms:size>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Pdf</ms:dataFormat>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormat>
							</ms:distributionTextFeature>
							<ms:licenceTerms>
								<ms:licenceTermsName xml:lang="en">ELRA-VAR-ACADEMIC-MEMBER-COMMERCIALUSE-1.0</ms:licenceTermsName>
								<ms:licenceTermsURL>http://www.elra.info/licenses/ELRA-VAR-ACADEMIC-MEMBER-COMMERCIALUSE-1.0.html</ms:licenceTermsURL>
							</ms:licenceTerms>
							<ms:availabilityStartDate>2016-01-20</ms:availabilityStartDate>
							<ms:distributionRightsHolder>
								<ms:Organization>
									<ms:actorType>Organization</ms:actorType>
									<ms:organizationName xml:lang="en">ELRA</ms:organizationName>
									<ms:website>http://www.elra.info/en/</ms:website>
								</ms:Organization>
							</ms:distributionRightsHolder>
						</ms:DatasetDistribution>
						<ms:DatasetDistribution>
							<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
							<ms:distributionTextFeature>
								<ms:size>
									<ms:amount>26443</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/entry</ms:sizeUnit>
								</ms:size>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Pdf</ms:dataFormat>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormat>
							</ms:distributionTextFeature>
							<ms:licenceTerms>
								<ms:licenceTermsName xml:lang="en">ELRA-END-USER-ACADEMIC-MEMBER-NONCOMMERCIALUSE-1.0</ms:licenceTermsName>
								<ms:licenceTermsURL>http://www.elra.info/licenses/ELRA-END-USER-ACADEMIC-MEMBER-NONCOMMERCIALUSE-1.0.html</ms:licenceTermsURL>
							</ms:licenceTerms>
							<ms:availabilityStartDate>2016-01-20</ms:availabilityStartDate>
							<ms:distributionRightsHolder>
								<ms:Organization>
									<ms:actorType>Organization</ms:actorType>
									<ms:organizationName xml:lang="en">ELRA</ms:organizationName>
									<ms:website>http://www.elra.info/en/</ms:website>
								</ms:Organization>
							</ms:distributionRightsHolder>
						</ms:DatasetDistribution>
						<ms:DatasetDistribution>
							<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
							<ms:distributionTextFeature>
								<ms:size>
									<ms:amount>26443</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/entry</ms:sizeUnit>
								</ms:size>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Pdf</ms:dataFormat>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormat>
							</ms:distributionTextFeature>
							<ms:licenceTerms>
								<ms:licenceTermsName xml:lang="en">ELRA-VAR-COMMERCIAL-MEMBER-COMMERCIALUSE-1.0</ms:licenceTermsName>
								<ms:licenceTermsURL>http://www.elra.info/licenses/ELRA-VAR-COMMERCIAL-MEMBER-COMMERCIALUSE-1.0.html</ms:licenceTermsURL>
							</ms:licenceTerms>
							<ms:availabilityStartDate>2016-01-20</ms:availabilityStartDate>
							<ms:distributionRightsHolder>
								<ms:Organization>
									<ms:actorType>Organization</ms:actorType>
									<ms:organizationName xml:lang="en">ELRA</ms:organizationName>
									<ms:website>http://www.elra.info/en/</ms:website>
								</ms:Organization>
							</ms:distributionRightsHolder>
						</ms:DatasetDistribution>
						<ms:DatasetDistribution>
							<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
							<ms:distributionTextFeature>
								<ms:size>
									<ms:amount>26443</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/entry</ms:sizeUnit>
								</ms:size>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Pdf</ms:dataFormat>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormat>
							</ms:distributionTextFeature>
							<ms:licenceTerms>
								<ms:licenceTermsName xml:lang="en">ELRA-END-USER-COMMERCIAL-MEMBER-NONCOMMERCIALUSE-1.0</ms:licenceTermsName>
								<ms:licenceTermsURL>http://www.elra.info/licenses/ELRA-END-USER-COMMERCIAL-MEMBER-NONCOMMERCIALUSE-1.0.html</ms:licenceTermsURL>
							</ms:licenceTerms>
							<ms:availabilityStartDate>2016-01-20</ms:availabilityStartDate>
							<ms:distributionRightsHolder>
								<ms:Organization>
									<ms:actorType>Organization</ms:actorType>
									<ms:organizationName xml:lang="en">ELRA</ms:organizationName>
									<ms:website>http://www.elra.info/en/</ms:website>
								</ms:Organization>
							</ms:distributionRightsHolder>
						</ms:DatasetDistribution>
						<ms:DatasetDistribution>
							<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
							<ms:distributionTextFeature>
								<ms:size>
									<ms:amount>26443</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/entry</ms:sizeUnit>
								</ms:size>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Pdf</ms:dataFormat>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormat>
							</ms:distributionTextFeature>
							<ms:licenceTerms>
								<ms:licenceTermsName xml:lang="en">ELRA-VAR-ACADEMIC-NOMEMBER-COMMERCIALUSE-1.0</ms:licenceTermsName>
								<ms:licenceTermsURL>http://www.elra.info/licenses/ELRA-VAR-ACADEMIC-NOMEMBER-COMMERCIALUSE-1.0.html</ms:licenceTermsURL>
							</ms:licenceTerms>
							<ms:availabilityStartDate>2016-01-20</ms:availabilityStartDate>
							<ms:distributionRightsHolder>
								<ms:Organization>
									<ms:actorType>Organization</ms:actorType>
									<ms:organizationName xml:lang="en">ELRA</ms:organizationName>
									<ms:website>http://www.elra.info/en/</ms:website>
								</ms:Organization>
							</ms:distributionRightsHolder>
						</ms:DatasetDistribution>
						<ms:DatasetDistribution>
							<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
							<ms:distributionTextFeature>
								<ms:size>
									<ms:amount>26443</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/entry</ms:sizeUnit>
								</ms:size>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Pdf</ms:dataFormat>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormat>
							</ms:distributionTextFeature>
							<ms:licenceTerms>
								<ms:licenceTermsName xml:lang="en">ELRA-END-USER-ACADEMIC-NOMEMBER-NONCOMMERCIALUSE-1.0</ms:licenceTermsName>
								<ms:licenceTermsURL>http://www.elra.info/licenses/ELRA-END-USER-ACADEMIC-NOMEMBER-NONCOMMERCIALUSE-1.0.html</ms:licenceTermsURL>
							</ms:licenceTerms>
							<ms:availabilityStartDate>2016-01-20</ms:availabilityStartDate>
							<ms:distributionRightsHolder>
								<ms:Organization>
									<ms:actorType>Organization</ms:actorType>
									<ms:organizationName xml:lang="en">ELRA</ms:organizationName>
									<ms:website>http://www.elra.info/en/</ms:website>
								</ms:Organization>
							</ms:distributionRightsHolder>
						</ms:DatasetDistribution>
						<ms:DatasetDistribution>
							<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
							<ms:distributionTextFeature>
								<ms:size>
									<ms:amount>26443</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/entry</ms:sizeUnit>
								</ms:size>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Pdf</ms:dataFormat>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormat>
							</ms:distributionTextFeature>
							<ms:licenceTerms>
								<ms:licenceTermsName xml:lang="en">ELRA-VAR-COMMERCIAL-NOMEMBER-COMMERCIALUSE-1.0</ms:licenceTermsName>
								<ms:licenceTermsURL>http://www.elra.info/licenses/ELRA-VAR-COMMERCIAL-NOMEMBER-COMMERCIALUSE-1.0.html</ms:licenceTermsURL>
							</ms:licenceTerms>
							<ms:availabilityStartDate>2016-01-20</ms:availabilityStartDate>
							<ms:distributionRightsHolder>
								<ms:Organization>
									<ms:actorType>Organization</ms:actorType>
									<ms:organizationName xml:lang="en">ELRA</ms:organizationName>
									<ms:website>http://www.elra.info/en/</ms:website>
								</ms:Organization>
							</ms:distributionRightsHolder>
						</ms:DatasetDistribution>
						<ms:DatasetDistribution>
							<ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
							<ms:distributionTextFeature>
								<ms:size>
									<ms:amount>26443</ms:amount>
									<ms:sizeUnit>http://w3id.org/meta-share/meta-share/entry</ms:sizeUnit>
								</ms:size>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Pdf</ms:dataFormat>
								<ms:dataFormat>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormat>
							</ms:distributionTextFeature>
							<ms:licenceTerms>
								<ms:licenceTermsName xml:lang="en">ELRA-END-USER-COMMERCIAL-NOMEMBER-NONCOMMERCIALUSE-1.0</ms:licenceTermsName>
								<ms:licenceTermsURL>http://www.elra.info/licenses/ELRA-END-USER-COMMERCIAL-NOMEMBER-NONCOMMERCIALUSE-1.0.html</ms:licenceTermsURL>
							</ms:licenceTerms>
							<ms:availabilityStartDate>2016-01-20</ms:availabilityStartDate>
							<ms:distributionRightsHolder>
								<ms:Organization>
									<ms:actorType>Organization</ms:actorType>
									<ms:organizationName xml:lang="en">ELRA</ms:organizationName>
									<ms:website>http://www.elra.info/en/</ms:website>
								</ms:Organization>
							</ms:distributionRightsHolder>
						</ms:DatasetDistribution>
						<ms:personalDataIncluded>false</ms:personalDataIncluded>
						<ms:sensitiveDataIncluded>false</ms:sensitiveDataIncluded>
					</ms:LexicalConceptualResource>
				</ms:LRSubclass>
			</ms:LanguageResource>
		</ms:DescribedEntity>
	</ms:MetadataRecord>

.. _uploadLexConc:

Step 2: upload
--------------

From the ELG catalogue, click the “Upload” link as shown below:

.. figure:: Upload01.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/
   :alt: Upload menu

Now upload the file you created in Step 1:

.. figure:: Upload02.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/
   :alt: Upload metadata XML

If there are any errors in your XML file, these will be shown to you. Fix them and try the upload again. Eventually, a success message will be shown to you and the metadata will be imported into the database.

Step 3: wait for approval
-------------------------

At this stage, the metadata record is only visible to you and to us, the ELG platform administrators. We will check your contribution and integrate it into the ELG catalogue if everything is in order, and contact you otherwise.