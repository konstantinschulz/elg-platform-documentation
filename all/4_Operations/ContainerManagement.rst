.. _containerManagement:


Container management
####################

This page presents useful tutorials on how to work with Kubernetes and Knative, the backbone and container management component of the ELG.


Setup Kubernetes and Helm
*************************

Getting started with Kubernetes
===============================

1. **Install** ``kubectl`` **on your machine**

Follow the instructions `here <https://kubernetes.io/docs/tasks/tools/install-kubectl>`_.

The recommended approach is via ``cURL``.

If you have troubles to get ``kubectl`` to your path, move the ``kubectl`` dir from your home to ``/usr/local/bin``:

.. code-block:: bash

    $ sudo mv /home/USERNAME/bin/kubectl /usr/local/bin/kubectl


2. **Copy the kubeconfig file** (get it from the maintainer of this repo) **to your** ``~/kube`` **directory**

3. **Run** ``$ kubectl cluster-info`` **to see if you can connect to the cluster**

4. **Create a proxy/tunnel**, e.g., in a different shell

``$ kubectl proxy``, then you can access the Kubernetes API in your browser.


5. **Create a token to login to the management console**

.. code-block:: bash

    $ kubectl -n kube-system describe secret admin-user-token-0815


6. **Login with your token in the browser** at: http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/overview?namespace=default


7. Try requesting some descriptions from our test service:

.. code-block:: bash

    $ kubectl describe pods/srv-langident


Installing Minikube
===================

Minikube is a standalone Kubernetes installation, useful for testing on your local system.

Minikube requires a local hypervisor to be installed (depending on your OS, choices include VirtualBox, KVM, Fusion, Hyper-V). On Linux, Docker can be used as a substitute for full virtualisation.
You need to refer to the installation instructions for your chosen hypervisor, e.g. for Docker they can be found `here <https://docs.docker.com/install/>`_.

Installation instructions for Minikube are available [here](https://kubernetes.io/docs/tasks/tools/install-minikube/).

When everything seems to be set up, run
`minikube start`.
If you used docker (so therefore you are on Linux), you need to specify an additional parameter `sudo minikube start --vm-driver=none` or use `minikube config set vm-driver none` to make this permanent.

This will start up a Kubernetes Cluster on your local system and set the default configs for kubectl to target that cluster ([see here to install kubectl](./Getting-started-with-kubernetes))
Running `kubectl cluster-info' should now be showing your local Kubernetes cluster to be online!

You also need to enable the ingress plugin, which is disabled on minukube by default:
`sudo minikube addons enable ingress`

Troubleshooting
^^^^^^^^^^^^^^^

If you run into:

.. code-block:: bash

    error: unable to read client-key /home/[user]/.minikube/client.key for minikube due to open /home/[user]/.minikube/client.key: permission denied

...move the ``client.key`` file as suggested in the minikube shell output or do a ``chown [user]: ~./minikube/client.key``



Installing Helm
===============

Please follow the instructions on `the official Helm website <https://helm.sh/docs/using_helm/#quickstart>`_.

If you want to target your local minikube, don't forget to install *socat*:

.. code-block:: bash

    sudo apt-get install socat


Using Kubernetes on Windows
===========================


1. The most convenient way is to first install `Docker Desktop for Windows 10 or later <https://www.docker.com/products/docker-desktop>`_. You need to register with Docker Hub. Afterwards, when Docker is running on your machine, open the settings by selecting them via a left-mouse click from the Docker icon in the Windows system tray. In the opening panel select *Kubernetes* and hook the *Enable Kubernetes* (You can also activate *Deploy Docker Stacks to Kubernetes by default*). The installation of Kubernetes takes a moment.

This will start up a Kubernetes Cluster on your local system and set the default configs for kubectl to target that cluster. Running ``kubectl cluster-info`` in the Windows PowerShell (you might want to run it as an admin) should now be showing your local Kubernetes cluster to be online!

2. Next, you need to install `Helm <https://github.com/helm/helm>`_, which is the package manager to organize Kubernetes configurations. You can install it under Windows via `Chocolatey <https://chocolatey.org/docs/installation>`_ which again is an installation manager for Windows. When you installed Chocolatey, you can simply tpye ``choco install kubernetes-helm`` into the Windows PowerShell.

3. Next, you want to check-out the grid from `our GitLab <https://gitlab.com/european-language-grid/platform/infra?nav_source=navbar>`_ (either via HTTP or SSH). Make sure that you keep the encoding between Windows/Linux machines consistent (all files checked into gitlab follow Linux line ending, you need to configure git to make automatic conversion).

4. Now, you need to run both ``deploy_core.sh`` and ``deploy_srv.sh``. Due to syntactical issues between Windows and Linux bash, this is not possible yet. Please, instead copy the steps from the scripts to your PowerShell and replace the ``$environ`` with ``local`` in each command. (Note: there is also a python3 script part.)

5. Finally, when everything worked well, you can now type ``kubectl get services --all-namespaces`` in your PowerShell and see a list of first services running. In the optimal case, opening ``https://localhost/local/srv-hello`` in your browser should return a successful response.

----

**Recommended:** :ref:`Frequently Asked Questions on k8s for service providers <k8sFAQ>`


Creating an image in GitLab
===========================

Prerequisite: Your service/tool must come with a std i/o messaging or a RESTful API, and that you create a docker file to build your tool/environment.

1. With your credentials, create a new project in your subproject under the `ELG GitLab group <https://gitlab.com/european-language-grid>`_ and commit your code there.

2. In your repository view, go to *+* and select *New file*. Then, in the next view, as a *Template* select *.gitlab-ci.yml* and template type (next field) select *Docker*. Your image will be built automatically.

3. Choosing *Registry* in the left menu bar, you can see the Docker registry link of your service. This link is what you must minimally offer in the ``srv-your-service-name.yaml`` that will be included (by an admin/semi-automatically) in `our infra repository <https://gitlab.com/european-language-grid/platform/infra/tree/master/elg-srv/srv/dev>`_.



Deploying the Grid locally
==========================

.. note:: This section will be provided shortly.



Useful Kubernetes commands for managing resources
=================================================

All of the following commands have to be prepended with

.. code-block:: console

    kubectl --kubeconfig <CONFIGFILE> -n <NAMESPACE>

where ``<CONFIGFILE>`` is your personal key that you have received from the admin team and ``<NAMESPACE>`` the namespace of the ELG cluster you are deploying on.

**List all kservices**

.. code-block:: console

    get kservice

**Delete kservice**

.. code-block:: console

    delete kservice service-srv-<SERVICEID>

where ``<SERVICEID>`` is the unique identifier of the kservice container.

**Inspect revision**

This is especially useful if your table entry in ``get kservice`` has a ``RevisionMissing`` tag. It will provide details that help to identify errors.

.. code-block:: console

    describe revisions service-srv-<SERVICEID>-<SUFFIX>

where ``SUFFIX`` is a 5-character long identifier of the revision.

**Inspect pod**

.. code-block:: console

    describe pod service-srv-<SERVICEID>-<SUFFIX>

**Inspect deployment of revision**

.. code-block:: console

    get deployment <SERVICEID> -o yaml


**Inspect REST server**

.. code-block:: console

    logs depl-restserver-<ID> --tail 30 -f -c restserver

where ID is the unique identifier of the REST server pod.


**Disable zero down-scaling**

For dynamic services, set:

.. code-block:: xml

    minScale: "1"

For static (non-kservice) services, set:

.. code-block:: xml

    replicas: "1"

...in the YAML file of the service in question.


**Force new revision**

In order to make the cluster fetch the newest revision of the service, you need to manually push events via GitLab.
Click on *Settings* -> *Webhooks*,
select the appropriate cluster (dev/live)
and click on *Push events*.
