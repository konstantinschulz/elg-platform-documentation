.. _operations:

ELG operations
##############

This chapter is for users who serve ELG in administrative roles, i.e., the ELG technical team. It is a growing collection of pages documenting internal processes in the interest of transparency and knowledge transfer.


Software development
********************

The following policies and best practices are observed for software development in the ELG project:

1. All software produced in the ELG project is developed using Git and (eventually) made available open-source via ELG’s GitLab group at https://gitlab.com/european-language-grid. Each partner can organise their code into one or multiple repositories. Only members of the ELG technical team have write access to the Git repositories.
2. Only libraries that are distributed under a FOSS license are used. Permissive licenses such as MIT, BSD, Apache and weak copyleft licenses such as LGPL are preferable to restrictive/copyleft licenses such as GPL, AGPL.
3. The contents of each repository are placed under an open-source license (TBD).
4. For planning and issue tracking, ELG’s Jira instance at https://european-language-grid.atlassian.net is used. Only members of the ELG technical team have access.
5. For documentation, we use (for now) ELG’s Read the Docs instance at https://european-language-grid.readthedocs.io.

