.. _typesOfResources:

Types of catalogue entries
############################


The `ELG catalogue <https://live.european-language-grid.eu/catalogue/#/>`_ includes:

- **Language resources and technologies (LRTs)**, further classified into:

   - **tools & services**: services that run in the cloud, but also downloadable tools, source code, etc.,
   - **corpora** a.k.a datasets: collections of text documents, audio transcripts, audio and video recordings, etc.,
   - models & computational grammars, collectively referred to as **language descriptions**,
   - **lexical/conceptual resources**, comprising computational lexica, gazetteers, ontologies, term lists, etc.

- related activities and stakeholders from the wider area of language technology:

   - **projects** that have funded the development of LRTs or in which they have been deployed,
   - **organizations**, as well as **groups** and **persons** active in language technology in Europe.

The following diagram depicts the taxonomy of catalogue entries:

.. image:: elgSchemaOverview.png
   :width: 800px
   :align: center
   :alt: Overview of ELG entities

.. note::  The current release doesn't display *groups* and *persons*. Moreover, *documents* and *licences* are described as separate entities, but they are not shown as main entities in the catalogue.