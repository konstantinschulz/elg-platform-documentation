.. _About:

Introduction
############

The `European Language Grid (ELG) <https://live.european-language-grid.eu>`_ is a catalogue and cloud platform that offers access to a **multitude of assets related to language technology (LT)**, including commercial and non-commercial cloud LT services for all European languages, data resources such as :ref:`models <contributeModel>`, :ref:`datasets <contributeCorpus>`, :ref:`lexica, terminologies <contributeLexConc>`, :ref:`grammars <contributeGrammar>`, as well as information on :ref:`LT-related projects <contributeProject>`, :ref:`organisations, and groups <contributeOrganisation>`.

This manual aims to guide 

- **consumers:** learn how to browse the ELG catalogue and find the language resources and technologies you need. Also find organisations and projects to connect with.

- **providers:** learn how to contribute your language resources and technologies to the ELG catalogue and how to host and sell access to your services via the ELG cloud.

- **moderators:** if you are part of the ELG technical team, learn how to review and ingest submitted language resources and technologies.

The current version of the manual documents the first official release of the ELG platform, launched in May 2020, which comes with a limited set of functionalities. More functionalities are continuously added, and this manual keeps on being updated following the evolution of the ELG platform.

If you have any questions or want to share feedback, please send an email to contact@european-language-grid.eu.