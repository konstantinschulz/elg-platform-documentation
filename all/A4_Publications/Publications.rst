.. _publications:

Publications and reports
########################

This annex contains research papers, reports and other documents that describe various aspects of the work carried out in the ELG project in detail.

Scientific publications
-----------------------

If you'd like to refer to the **European Language Grid initiative and platform** in a general way, please cite the following article.

Georg Rehm, Maria Berger, Ela Elsholz, Stefanie Hegele, Florian Kintzel, Katrin Marheinecke, Stelios Piperidis, Miltos Deligiannis, Dimitris Galanis, Katerina Gkirtzou, Penny Labropoulou, Kalina Bontcheva, David Jones, Ian Roberts, Jan Hajic, Jana Hamrlová, Lukáš Kačena, Khalid Choukri, Victoria Arranz, Andrejs Vasiļjevs, Orians Anvari, Andis Lagzdiņš, Jūlija Meļņika, Gerhard Backfried, Erinç Dikici, Miroslav Janosik, Katja Prinz, Christoph Prinz, Severin Stampler, Dorothea Thomas-Aniola, José Manuel Gómez Pérez, Andres Garcia Silva, Christian Berrío, Ulrich Germann, Steve Renals, Ondrej Klejch. `European Language Grid: An Overview <https://www.aclweb.org/anthology/2020.lrec-1.413/>`_. In Nicoletta Calzolari, Frédéric Béchet, Philippe Blache, Christopher Cieri, Khalid Choukri, Thierry Declerck, Hitoshi Isahara, Bente Maegaard, Joseph Mariani, Asuncion Moreno, Jan Odijk, and Stelios Piperidis, editors, `Proceedings of the 12th Language Resources and Evaluation Conference (LREC 2020) <https://www.aclweb.org/anthology/volumes/2020.lrec-1/>`_, pages 3359-3373, Marseille, France, 2020. European Language Resources Association (ELRA). [`.bib <https://www.aclweb.org/anthology/2020.lrec-1.413.bib>`_ | `.pdf <https://www.aclweb.org/anthology/2020.lrec-1.413.pdf>`_]

If you'd like to refer to the **metadata schema developed for and used in the European Language Grid platform**, please cite the following article.

Penny Labropoulou, Katerina Gkirtzou, Maria Gavriilidou, Miltos Deligiannis, Dimitris Galanis, Stelios Piperidis, Georg Rehm, Maria Berger, Valérie Mapelli, Michael Rigault, Victoria Arranz, Khalid Choukri, Gerhard Backfried, José Manuel Gómez Pérez, and Andres Garcia-Silva.
`Making Metadata Fit for Next Generation Language Technology Platforms: The Metadata Schema of the European Language Grid <https://www.aclweb.org/anthology/2020.lrec-1.420/>`_.
In Nicoletta Calzolari, Frédéric Béchet, Philippe Blache, Christopher Cieri, Khalid Choukri, Thierry Declerck, Hitoshi Isahara, Bente Maegaard, Joseph Mariani, Asuncion Moreno, Jan Odijk, and Stelios Piperidis, editors, `Proceedings of the 12th Language Resources and Evaluation Conference (LREC 2020) <https://www.aclweb.org/anthology/volumes/2020.lrec-1/>`_, pages 3421-3430, Marseille, France, 2020. European Language Resources Association (ELRA). [`.bib <https://www.aclweb.org/anthology/2020.lrec-1.420.bib>`_ | `.pdf <https://www.aclweb.org/anthology/2020.lrec-1.420.pdf>`_]

If you'd like to refer to a current description of the **situation of the wider Multilingual Europe community**, please cite the following article.

Georg Rehm, Katrin Marheinecke, Stefanie Hegele, Stelios Piperidis, Kalina Bontcheva, Jan Hajic, Khalid Choukri, Andrejs Vasiļjevs, Gerhard Backfried, Christoph Prinz, José Manuel Gómez Pérez, Luc Meertens, Paul Lukowicz, Josef van Genabith, Andrea Lösch, Philipp Slusallek, Morten Irgens, Patrick Gatellier, Joachim Köhler, Laure Le Bars, Dimitra Anastasiou, Albina Auksoriūtė, Núria Bel, António Branco, Gerhard Budin, Walter Daelemans, Koenraad De Smedt, Radovan Garabík, Maria Gavriilidou, Dagmar Gromann, Svetla Koeva, Simon Krek, Cvetana Krstev, Krister Lindén, Bernardo Magnini, Jan Odijk, Maciej Ogrodniczuk, Eiríkur Rögnvaldsson, Mike Rosner, Bolette Pedersen, Inguna Skadina, Marko Tadić, Dan Tufiș, Tamás Váradi, Kadri Vider, Andy Way, and François Yvon.
`The European Language Technology Landscape in 2020: Language-Centric and Human-Centric AI for Cross-Cultural Communication in Multilingual Europe <https://www.aclweb.org/anthology/2020.lrec-1.407/>`_.
In Nicoletta Calzolari, Frédéric Béchet, Philippe Blache, Christopher Cieri, Khalid Choukri, Thierry Declerck, Hitoshi Isahara, Bente Maegaard, Joseph Mariani, Asuncion Moreno, Jan Odijk, and Stelios Piperidis, editors, `Proceedings of the 12th Language Resources and Evaluation Conference (LREC 2020) <https://www.aclweb.org/anthology/volumes/2020.lrec-1/>`_, pages 3315-3325, Marseille, France, 2020. European Language Resources Association (ELRA). [`.bib <https://www.aclweb.org/anthology/2020.lrec-1.407.bib>`_ | `.pdf <https://www.aclweb.org/anthology/2020.lrec-1.407.pdf>`_]


**Proceedings of the 1st International Workshop on Language Technology Platforms**

Georg Rehm, Kalina Bontcheva, Khalid Choukri, Jan Hajic, Stelios Piperidis, and Andrejs Vasiljevs, editors.
`Proceedings of the 1st International Workshop on Language Technology Platforms <https://www.aclweb.org/anthology/volumes/2020.iwltp-1/>`_
(IWLTP 2020, co-located with LREC 2020), Marseille, France, 2020. 16 May 2020. [`.bib <https://www.aclweb.org/anthology/volumes/2020.iwltp-1.bib>`_ | `.pdf <https://www.aclweb.org/anthology/2020.iwltp-1.0.pdf>`_]


If you'd like to refer to our initial thoughts and plans **how to make a number of European AI platforms interoperable**, please cite the following article.

Georg Rehm, Dimitrios Galanis, Penny Labropoulou, Stelios Piperidis, Martin Welß, Ricardo Usbeck, Joachim Köhler, Miltos Deligiannis, Katerina Gkirtzou, Johannes Fischer, Christian Chiarcos, Nils Feldhus, Julián Moreno-Schneider, Florian Kintzel, Elena Montiel, Víctor Rodríguez Doncel, John P. McCrae, David Laqua, Irina Patricia Theile, Christian Dittmar, Kalina Bontcheva, Ian Roberts, Andrejs Vasiljevs, and Andis Lagzdiņš.
`Towards an Interoperable Ecosystem of AI and LT Platforms: A Roadmap for the Implementation of Different Levels of Interoperability <https://www.aclweb.org/anthology/2020.iwltp-1.15/>`_.
In Georg Rehm, Kalina Bontcheva, Khalid Choukri, Jan Hajic, Stelios Piperidis, and Andrejs Vasiljevs, editors, `Proceedings of the 1st International Workshop on Language Technology Platforms (IWLTP 2020, co-located with LREC 2020) <https://www.aclweb.org/anthology/volumes/2020.iwltp-1/>`_, pages 96-107, Marseille, France, 2020. 16 May 2020. [`.bib <https://www.aclweb.org/anthology/2020.iwltp-1.15.bib>`_ | `.pdf <https://www.aclweb.org/anthology/2020.iwltp-1.15.pdf>`_]



Related publications
--------------------

Andrus Ansip. `How multilingual is Europe’s Digital Single Market? <https://medium.com/@JochenHummel/how-multilingual-is-europes-digital-single-market-9b8d908fce6c>`_. 2016.

Communication from the commission to the European Parliament, the coucil, the European econimic and social committee and the committee of the reagions. `A Digital Single Market Strategy for Europe <https://ec.europa.eu/digital-single-market/en/news/digital-single-market-strategy-europe-com2015-192-final>`_. COM (Communication), 192. Brussels, Belgium, 2015.

European Parliament. `Report on language equality in the digital age. <http://www.europarl.europa.eu/doceo/document/A-8-2018-0228_EN.html>`_. Jill Evans, rapporteur, Committee on Culture and Education (CULT), Committee on Industry, Research and Energy (ITRE). Strasbourg, France, 2018.

Eurostat. `Internet access and use statistics – households and individuals. <https://ec.europa.eu/eurostat/statistics-explained/index.php?title=Archive:Internet_access_and_use_statistics_-_households_and_individuals>`_. 2016.

Andreás Kornai. `Digital Language Death <https://doi.org/10.1371/journal.pone.0077056>`_. In PLoS ONE, volume 8, 2013.

Georg Rehm and Stefanie Hegele. `Language Technology for Multilingual Europe: An Analysis of a Large-Scale Survey regarding Challenges, Demands, Gaps and Needs <https://www.aclweb.org/anthology/L18-1519/>`_. In Nicoletta Calzolari, Khalid Choukri, Christopher Cieri, Thierry Declerck, Sara Goggi, Koiti Hasida, Hitoshi Isahara, Bente Maegaard, Joseph Mariani, Hélène Mazo, Asuncion Moreno, Jan Odijk, Stelios Piperidis, Takenobu Tokunaga, editors, 
`Proceedings of the 11th Language Resources and Evaluation Conference (LREC 2018) <https://www.aclweb.org/anthology/volumes/L18-1/>`_, pages 3282–3289, Miyazaki, Japan, 2018. 
European Language Resources Association (ELRA). [`.bib <https://www.aclweb.org/anthology/L18-1519.bib>`_ | `.pdf <https://www.aclweb.org/anthology/L18-1519.pdf>`_]

Georg Rehm, Hans Uszkoreit, editors. `META-NET White Paper Series: Europe’s Languages in the Digital Age, 32 volumes on 31 European languages <http://www.meta-net.eu/whitepapers/overview>`_. 2012. Springer, Heidelberg.

Georg Rehm and Hans Uszkoreit, editors. `The META-NET Strategic Research Agenda for Multilingual Europe 2020 <http://www.meta-net.eu/sra>`_.
Dodrecht, New York, London, 2013. Springer, Heidelberg.

Georg Rehm, Hans Uszkoreit, Ido Dagan, Vartkes Goetcherian, Mehmet Ugur Dogan, Coskun Mermer, Tamás Váradi, Sabine Kirchmeier-Andersen, Gerhard Stickel, Meirion Prys Jones, Stefan Oeter and Sigve Gramstad. `An Update and Extension of the META-NET Study “Europe’s Languages in the Digital Age” <https://www.dfki.de/web/forschung/projekte-publikationen/publikationen-uebersicht/publikation/7489/>`_. 
In Laurette Pretorius, Claudia Soria, Paola Baroni, editors, `Proceedings of the Workshop on Collaboration and Computing for Under-Resourced Languages in the Linked Open Data Era (CCURL 2014) <http://www.lrec-conf.org/proceedings/lrec2014/index.html>`_, 
pages 30–37, Reykjavik, Iceland, 2014.

Georg Rehm, Jan Hajič, Josef van Genabith and Andrejs Vasiljevs. `Fostering the Next Generation of European Language Technology: Recent Developments – Emerging Initiatives – Challenges and Opportunities <https://www.aclweb.org/anthology/L16-1251/>`_. 
Nicoletta Calzolari, Khalid Choukri, Thierry Declerck, Sara Goggi, Marko Grobelnik, Bente Maegaard, Joseph Mariani, Helene Mazo, Asuncion Moreno, Jan Odijk, Stelios Piperidis, editors, `Proceedings of the 10th Language Resources and Evaluation Conference (LREC 2016) <https://www.aclweb.org/anthology/volumes/L16-1/>`_, pages 1586–1592, 
Portorož, Slovenia, 2016. European Language Resources Association (ELRA). [`.bib <https://www.aclweb.org/anthology/L16-1251.bib>`_ | `.pdf <https://www.aclweb.org/anthology/L16-1251.pdf>`_]

Rehm, G., editor. `Language Technologies for Multilingual Europe: Towards a Human Language Project <http://www.cracking-the-language-barrier.eu/wp-content/uploads/SRIA-V1.0-final.pdf>`_. 
Strategic Research and Innovation Agenda. Unveiled at `META-FORUM 2017 <http://www.meta-net.eu/events/meta-forum-2017>`_. Prepared by the Cracking the Language Barrier federation, 
supported by the EU project CRACKER. Brussels, Belgium, 2017.

Georg Rehm, Hans Uszkoreit, Sophia Ananiadou, Núria Bel, Audronė Bielevičienė, Lars Borin, 
António Branco, Gerhard Budin, Nicoletta Calzolari, Walter Daelemans, Radovan Garabík, 
Marko Grobelnik, Carmen García-Mateo, Josef van Genabith, Jan Hajič, Inma Hernáez, John Judge, 
Svetla Koeva, Simon Krek, Cvetana Krstev, Krister Lindén, Bernardo Magnini, Joseph Mariani, John McNaught, 
Maite Melero, Monica Monachini, Asunción Moreno, Jan Odijk, Maciej Ogrodniczuk, Piotr Pęzik, 
Stelios Piperidis, Adam Przepiórkowski, Eiríkur Rögnvaldsson, Michael Rosner, 
Bolette Pedersen, Inguna Skadiņa, Koenraad De Smedt, Marko Tadić, Paul Thompson, Dan Tufiş, 
Tamás Váradi, Andrejs Vasiļjevs, Kadri Vider and Jolanta Zabarskaite.
`The strategic impact of META-NET on the regional, national and international level <https://www.aclweb.org/anthology/L14-1350/>`_. Lang. Resour. Evaluation, 50(2),
In Nicoletta Calzolari, Khalid Choukri, Thierry Declerck, Hrafn Loftsson, Bente Maegaard, Joseph Mariani, Asuncion Moreno, Jan Odijk, Stelios Piperidis, Editors. `Proceedings of the Ninth International Conference on Language Resources and Evaluation 2014 (LREC'14), <https://www.aclweb.org/anthology/volumes/L14-1/>`_.
pages 1517–1524, Reykjavik, Iceland, 2014.
European Language Resources Association (ELRA). [`.bib <https://www.aclweb.org/anthology/L14-1350.bib>`_ | `.pdf <https://www.aclweb.org/anthology/L14-1350.pdf>`_]

`Riga Declaration, Declaration of Common Interests <http://rigasummit2015.eu>`_. Prepared and signed by 12 language and language technology stakeholders at the
Riga Summit 2015 on the Multilingual Digital Single Market. 2015.

Benjamin Sargent. `The 116 Most Economically Active Languages Online <https://www.commonsenseadvisory.com/AbstractView/tabid/74/ArticleID/5590/Title/The116MostEconomicallyActiveLanguagesOnline/Default.aspx>`_. CommonSenseAdvisory. 2013.

Rafael Rivera Pastor, Carlotta Tarín Quirós, Juan Pablo Villar García, Toni Badia Cardús and Maite Melero Nogués. `Language equality in the digital age – Towards a Human Language Project <https://www.europarl.europa.eu/stoa/en/document/EPRS_STU(2017)598621>`_. A study of the Scientific Foresight Unit (STOA) carried out by Iclaves within the Directorate-General for Parliamentary Research Services (DG EPRS) of the European Parliament. 2017.

Andrejs Vasiljevs, Khalid Choukri, Luc Meertens and Stefani Aguzzi. `Final study report on CEF Automated Translation value proposition in the context of the European LT market/ecosystem <https://op.europa.eu/de/publication-detail/-/publication/8494e56d-ef0b-11e9-a32c-01aa75ed71a1/language-en>`_. A study prepared for the European Commission, DG Communications Networks, Content & Technology by Crosslang, Tilde, ELDA, IDC. Luxembourg. 2019.

Roberto Viola and Rytis Martikonis. `Multilingualism in the Digital Age: a barrier or an opportunity <https://ec.europa.eu/digital-single-market/en/blog/multilingualism-digital-age-barrier-or-opportunity>`_. 2017.


Deliverables
------------

.. note:: These publications will be provided shortly.