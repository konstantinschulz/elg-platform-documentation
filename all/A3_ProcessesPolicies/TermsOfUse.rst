.. _termsOfUse:

Terms of use
############

Use of the European Language Grid is subject to its `terms of use <https://live.european-language-grid.eu/page/terms-of-use>`_.
