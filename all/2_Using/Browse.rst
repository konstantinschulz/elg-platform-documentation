.. _browse:

Browse the catalogue
##############################################


View catalogue 
---------------

You can 

- browse through the catalogue and see all the entries,
- search for entries using the free text bar,
- filter entries by type, language, service function, license, etc.

.. figure:: Catalogue.png
    :width: 800px
    :align: center
    :target: https://live.european-language-grid.eu/catalogue/#/
    :alt: The catalogue view

By clicking on the name of an entry, you can view its detailed description. 

.. _viewmr:

View catalogue entry
--------------------

For each catalogue entry, we display a set of descriptive and technical information (:ref:`metadata <basicSchema>`), together with hyperlinks to supporting documentation and other useful material.

The following figure shows the catalogue entry for a **tool/service**. 

.. figure:: viewPageService.png
    :width: 800px
    :align: center
    :target: https://live.european-language-grid.eu/catalogue/#/resource/service/tool/474
    :alt: View page of a tool/service

There are four tabs:

- **Overview**: contains the main metadata (e.g., description of basic features, function, input and output language(s) and data format(s), etc.), links to supporting documentation, contact details, resource providers, etc. 
- **Download/Run**: includes the licensing terms under which the tool/service can be accessed, and relevant technical information (i.e., whether it can be downloaded and executed locally, is provided with source code, etc.). 
- **Try out** (only for services running in the cloud): you can provide a sample input and see the results output by the service. Depending on the type of the service, you can type in or paste some text, upload an audio file or record something, etc., and get the results rendered in a task-specific viewer.
- **Code samples** (only for services running in the cloud): you can use the code sample/template provided to test the service from the command line.

The following figure shows the entry of a **corpus**.

.. figure:: viewPageCorpus.png
    :width: 800px
    :align: center
    :target: https://live.european-language-grid.eu/catalogue/#/resource/service/corpus/652
    :alt: View page of a corpus

There are three tabs:

- **Overview**: contains the main metadata: description, subclass, keyword(s), domain(s), etc., as well as links to supporting documentation, contact details, resource providers, etc. Some properties are grouped under the "parts" of a resource, each of which is characterised by the media type (text, audio, video, image). This allows us, for example, to describe a multimedia corpus of videos, their audio excerpts (in English), the transcriptions of the recordings (in an annotated format), and the subtitles in one or more languages (English and French, provided in plain text files), as a set of four distinct parts with the corresponding properties.
- **Download**: The second tab includes the licensing terms under which the resource can be accessed, and technical details on how it can be accessed (i.e., whether it can be downloaded, used via an interface, etc.), as well as details on formats and size. If the resource has been uploaded to ELG, you will also be able to download it directly; otherwise, you will be re-directed to the original access location. 
- **General**: This tab appears only for resources with a rich description and is used so as not to make the first tab too long and difficult to read.

The next two figures show the entries for **lexical/conceptual resources** (lexica, terminologies, ontologies, etc.) and **language descriptions** respectively, with information tabs similar to those of corpora.

.. figure:: viewPageLexicalConceptualResource.png
    :width: 800px
    :align: center
    :target: https://live.european-language-grid.eu/catalogue/#/resource/service/lcr/916
    :alt: View page of a lexical/conceptual resource

.. figure:: viewPageLanguageDescription.png
    :width: 800px
    :align: center
    :target: https://live.european-language-grid.eu/catalogue/#/resource/service/ld/904
    :alt: View page of a language description

The last two figures show respectively the catalogue entries for an **organization** and a **project**, with contact details, funding information, links to resources, etc.

.. figure:: viewPageOrganization.png
    :width: 800px
    :align: center
    :target: https://live.european-language-grid.eu/catalogue/#/resource/organizations/384
    :alt: View page of an organization

.. figure:: viewPageProject.png
    :width: 800px
    :align: center
    :target: https://live.european-language-grid.eu/catalogue/#/resource/projects/395
    :alt: View page of a project