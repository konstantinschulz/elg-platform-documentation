.. _Using:

Using ELG
#########

This chapter is for **consumers**, i.e. users of the European Language Grid who wish to explore the catalogue and use the provided language resources and technologies (LRTs). You will learn how to use the catalogue, how to try out and how to download LRTs.