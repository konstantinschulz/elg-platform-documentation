.. _register:

Register as a user
##################

Although you can browse the catalogue and access many resources without registration, a user account is required for, e.g., accessing resources with access restrictions, or making use of the try-out feature.

If you don’t have an account yet, request one via our `contact form <https://www.european-language-grid.eu/contact/>`_.

To sign in to your existing account, click on the user icon at the top right and follow the instructions.
