.. toctree::
   :maxdepth: 2
   :caption: Chapter 4: ELG Operations

   ./all/4_Operations/Operations.rst
   ./all/4_Operations/Helpdesk.rst
   ./all/4_Operations/AccessManagement.rst
   ./all/4_Operations/ValidateResource.rst
   ./all/4_Operations/CatalogueAdmin.rst
   ./all/4_Operations/ContainerManagement.rst
   ./all/4_Operations/ClusterAdmin.rst

   ./all/A3_ProcessesPolicies/ProcessesPolicies.rst
